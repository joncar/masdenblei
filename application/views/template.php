<!DOCTYPE html>
<html lang="en">	
	<head>
		<title><?= empty($title) ? 'Mas Den Blei' : $title ?></title>
		<meta charset="utf-8">
	  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
		<meta name="description" content="<?= empty($description) ?'': $description ?>" /> 	
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<script>window.wURL = window.URL; var URL = '<?= base_url() ?>'; var lang = '<?= $_SESSION['lang'] ?>';</script>
		<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
		<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
		<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>

		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900' type='text/css' media='all' />
		<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600,600i,700,700i' type='text/css' media='all' />
		<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
	

		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/css/font-icons/css/trx_addons_icons-embedded.css' type='text/css' media='all' />
		
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/swiper/swiper.min.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/magnific/magnific-popup.min.css' type='text/css' media='all' />

		<link rel='stylesheet' id='rs-plugin-settings-css' href='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/css/settings.css' type='text/css' media='all' />
		
		<link rel='stylesheet' id='tribe-events-full-calendar-style-css' href='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/css/tribe-events-full.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='tribe-events-calendar-style-css' href='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/css/tribe-events-theme.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='tribe-events-calendar-full-mobile-style-css' href='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/css/tribe-events-full-mobile.min.css' type='text/css' media='only screen and (max-width: 768px)' />
		<link rel='stylesheet' id='tribe-events-calendar-mobile-style-css' href='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/css/tribe-events-theme-mobile.min.css' type='text/css' media='only screen and (max-width: 768px)' />


		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/css/trx_addons.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/js/vendor/plugins/woocommerce/assets/css/woocommerce-layout.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/js/vendor/plugins/woocommerce/assets/css/woocommerce-smallscreen.css' type='text/css' media='only screen and (max-width: 768px)' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/js/vendor/plugins/woocommerce/assets/css/woocommerce.css' type='text/css' media='all' />
		<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Crimson+Text:400,400italic,600,600italic,700,700italic|Lato:400,700&amp;subset=latin,latin-ext' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/fontello/css/fontello-embedded.css' type='text/css' media='all' />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">   
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/style.min.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/animation.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/__colors.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/__styles.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/custom.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/responsive.min.css' type='text/css' media='all' />
		<link rel='stylesheet' href='<?= base_url() ?>js/lightgallery/css/lightgallery.min.css' type='text/css' media='all' />
	</head>	
	<?php $this->load->view($view); ?>			
</html>