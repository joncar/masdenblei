<div class="loaded" id="page-content"> 
    <header class="overlay"> <!-- navigation / main menu --> 
        <?= $this->load->view('includes/template/menu2') ?>
    </header> <!-- main content --> 
    <main> 
        <section style="padding-top: 117px; height: 100%;"> 
            <div class="container">
                <h1>Zona Usuario</h1>
                <div class="row" style="margin:20px;">
                    <div class="col-xs-12 col-sm-2">
                        <img src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="<?= $this->user->nombre ?>" style="width:100%; border:5px solid #f71259"/>
                    </div>
                    <div class="col-xs-12 col-sm-8 panelUsuarioDatos" style="border-right:5px solid #f71259;">
                        <ul class="list-group">
                            <li class="list-group-item"><b>Nombre: </b><?= $this->user->nombre ?></li>
                            <li class="list-group-item"><b>Apellido: </b><?= $this->user->apellido ?></li>
                            <li class="list-group-item"><b>Centro: </b><?= $this->user->centro ?></li>
                            <li class="list-group-item"><b>Curso: </b><?= $this->user->curso ?></li>
                            <li class="list-group-item"><b>Grupo: </b><?= $this->user->nro_grupo ?></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <ul class="list-group">
						<li class="list-group-item" style="background-color: #f72859; color: white;">Documentos</li>                            
						<?php foreach($this->db->get('documentos')->result() as $d): ?>
                            <li class="list-group-item"><a href="<?= base_url('files/'.$d->file) ?>" target="_new"><?= $d->nombre ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="row" style="margin:20px;">
                    <h2>Instrucciones</h2>
                    <p>Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate </p>
                </div>
                <div class="row" style="margin:20px;">
                    <?= $output ?>
                </div>
            </div> 
        </section> <!-- go up arrow --> 
        <button class="btn goUp-btn"> 
            <i class="fa fa-angle-up"></i> <span>Go Up</span><span class="mydiv">variolitic</span> 
        </button> <!-- /.go up arrow --> 
        <?php $this->load->view('includes/scripts',array('removeFunction'=>true)); ?>
    </main>
    <form id="formPay" action="http://tpv.ceca.es:8000/cgi-bin/tpv" method="post" enctype="application/x-www-form-urlencoded">
        <input name="MerchantID" type=hidden value="081534372">
        <input name="AcquirerBIN" type=hidden value="0000554026">
        <input name="TerminalID" type=hidden value="00000003">
        <input name="URL_OK" type=hidden value="<?= base_url('pagos/frontend/pagook') ?>">
        <input name="URL_NOK" type=hidden value="<?= base_url('pagos/frontend/pagonok') ?>">
        <input id="firma" name="Firma" type=hidden value="">
        <input name="Cifrado" type=hidden value="SHA1">
        <input id="operacion" name="Num_operacion" type=hidden value="">
        <input id="importe" name="Importe" type=hidden value="">
        <input name="TipoMoneda" type=hidden value="978">
        <input name="Exponente" type=hidden value="2">
        <input name="Pago_soportado" type=hidden value="SSL">
        <input name="Idioma" type=hidden value="1">
    </form>
</div>
<script>
    $('#crudForm').on('success',function(ev,e){
        tokenizar(e);
    });
    
    function tokenizar(id){
        $.post('<?= base_url('pagos/admin/tokenizar_pago') ?>',{id:id},function(data){
            data = JSON.parse(data);
            $("#importe").val(data.importe);
            $("#operacion").val(data.id);
            $("#firma").val(data.firma);
            $("#formPay").submit();
        });
    }
    
    
</script>