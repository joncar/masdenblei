<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	

	function vinos($where = array()){
		$this->db->order_by('orden','ASC');		
		switch($_SESSION['lang']){
			case 'ca': $web = 'vi/';
			break;
			case 'es': $web = 'vino/';
			break;
			case 'en': $web = 'wine/';
			break;
		}
		$gal = $this->db->get_where('vinos',$where);
		foreach($gal->result() as $n=>$v){
			$gal->row($n)->foto = base_url('img/vinos/'.$v->foto);
			$gal->row($n)->foto_lateral = base_url('img/vinos/'.$v->foto_lateral);
			$gal->row($n)->fondo = base_url('img/vinos/'.$v->fondo);
			$gal->row($n)->link = base_url().$web.toUrl($v->enlace);
			$gal->row($n)->ampollas = explode(',',$v->botellas);
		}
		$gal = $this->traduccion->transform($gal);
		return $gal;
	}

	function distribuidores($where = array()){
		$this->db->order_by('orden','ASC');			
		$this->db->select("distribuidores.*, MapToData('Lat',mapa) as lat, MapToData('Lng',mapa) as lng",FALSE);	
		$gal = $this->db->get_where('distribuidores',$where);
		foreach($gal->result() as $n=>$v){
			
		}
		$gal = $this->traduccion->transform($gal);
		return $gal;
	}

	function galeria($where = array()){
		$this->db->order_by('orden','ASC');					
		$gal = $this->db->get_where('galeria',$where);
		foreach($gal->result() as $n=>$v){			
			$gal->row($n)->foto = base_url('img/galeria/'.$v->foto);
		}
		return $gal;
	}

	function premios($where = array()){		
		$gal = $this->db->get_where('premios',$where);
		foreach($gal->result() as $n=>$v){			
			$gal->row($n)->foto = base_url('img/vinos/'.$v->foto);
			$gal->row($n)->logo_premio = base_url('img/vinos/'.$v->logo_premio);
		}
		$gal = $this->traduccion->transform($gal);
		return $gal;
	}






	function toArray($obj,$field = 'id'){
		$data = array();
		foreach($obj->result() as $o){			
			$data[$o->{$field}] = $o;
		}
		return $data;
	}

	function blog(){						
		$this->db->order_by('fecha','DESC');
		//$this->db->where('idioma',$_SESSION['lang']);
		$blog = $this->db->get_where('blog',array('status'=>1));
		foreach($blog->result() as $nn=>$b){
			$blog->row($nn)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
	        $blog->row($nn)->foto = base_url('img/blog/'.$b->foto);
	        $blog->row($nn)->fecha = strftime('%d %b %Y',strtotime($b->fecha));
	        $blog->row($nn)->texto = strip_tags($b->texto);
	        $blog->row($nn)->texto = cortar_palabras($b->texto,25);
	        $blog->row($nn)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();            
	        $blog->row($nn)->descripcion = cortar_palabras(strip_tags($b->texto),20);	
	        $blog->result_object[$nn] = $this->traduccion->traducirObj($b);
	        if($blog->result_object[$nn]->idioma != $_SESSION['lang']){
	        	unset($blog->result_object[$nn]);
	        }
		}
		return $blog;
	}

	function fechas_bloqueadas(){	
		$this->db->where('fecha >=',date("Y-m-d"));					
		$blog = $this->db->get_where('fechas_bloqueadas',array());
		return $blog;
	}
}