<?php 
	$idiomas = $this->db->get_where('ajustes')->row()->idiomas; 
	$idiomas = explode(', ',$idiomas);	
	$next = array();
	for($i=1;$i<count($idiomas);$i++){
		$next[] = $idiomas[$i];
	}
	echo '<script>var idiomas = '.json_encode($next).'; var texts = [];</script>';
?>
<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <?php foreach($idiomas as $n=>$i): ?>
	    <li role="presentation" class="<?= $n==0?'active':'' ?>">
	    	<a href="#tab<?= $i ?>" aria-controls="home" role="tab" data-toggle="tab"><?= $i ?></a>
	    </li>    
	<?php endforeach ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
  	<?php foreach($idiomas as $n=>$i): ?>
	    <div role="tabpanel" class="tab-pane <?= $n==0?'active':'' ?>" id="tab<?= $i ?>">
	    	<?php if($n==0)echo $output; else echo 'Cargando idioma' ?>
	    </div>
	<?php endforeach ?>
  </div>

</div>

<script>
	$(document).on('ready',function(){
		var form = $("#tab<?= $idiomas[0] ?>").find('.flexigrid form');		
		var str = '';
		var x = 0;
		form.find('input[type="text"],textarea').each(function(){

			x++;
			if(x==form.find('input[type="text"],textarea').length){
				for(var i in idiomas){
					$("#tab"+idiomas[i]).html(str);
				}
			}
		});		
	});	

	function initBind(el){
		var str = '';
		for(var i in idiomas){
			str+= '#tab'+idiomas[i]+' input[type="text"],textarea';
		}
		console.log(str);
		el.find('input[type="text"],textarea').on('change',function(){
			
		});
	}
</script>