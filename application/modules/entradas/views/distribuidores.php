<?= $output ?>

<script>

	var input = document.getElementById('field-direccion'); 
	var autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
	autocomplete.addListener('place_changed', fillInAddress);
	console.log(mapa.marker);
	function fillInAddress(){
	    var place = autocomplete.getPlace().geometry.location;    
	    mapa.marker.setPosition(new google.maps.LatLng(place.lat(),place.lng()));
	    mapa.map.panTo(new google.maps.LatLng(place.lat(),place.lng()));
	    $("#field-mapa").val(mapa.marker.getPosition());
	    //google.maps.event.addListener(mapa.marker,'dragend',function(){$("#field-ubicacion").val(ubicacion.marker.getPosition())});
	}

	function searchDireccion(direccion){   
	        var geocoder = new google.maps.Geocoder();
	        var address = direccion;
	        geocoder.geocode({'address': address}, function(results, status) {
	                if (status === google.maps.GeocoderStatus.OK) {
	                    map.setCenter(results[0].geometry.location);
	                    for(var i in results){                                    
	                        CrearMarcas(results[i]);
	                    }
	                    if(scope.lugares!==undefined){
	                        scope.lugares(results);
	                    }
	                } else {
	                  alert('No se ha podido encontrar la ubicación indicada: ' + status);
	                  window.history.back();
	                }
	        });
	}
</script>