<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }

        function vinos(){
        	$crud = $this->crud_function('','');
        	$crud->display_as('botellas','Botellas (Separates por coma)');
        	$crud->set_order('orden');
        	$crud->field_type('foto','image',array('path'=>'img/vinos','width'=>'960px','height'=>'954px'))
        		 ->field_type('foto_lateral','image',array('path'=>'img/vinos','width'=>'2340px','height'=>'1560px'))
        		 ->field_type('fondo','image',array('path'=>'img/vinos','width'=>'2340px','height'=>'1560px'))        		 
                 ->field_type('idiomas','hidden')
        		 ->columns('foto','anyo','enlace','nombre')
        		 ->set_clone();  
            $crud->add_action('Traducir','',base_url('entradas/admin/vinos/traducir').'/');                  
        	$out = $crud->render();
        	$this->loadView($out);
        }

        function distribuidores(){
            $crud = $this->crud_function('','');            
            $crud->set_order('orden')            
                 ->set_clone();         
            $crud->field_type('mapa','map',array('width'=>'500px','height'=>'500px'))
                 ->field_type('region','dropdown',array('1'=>'Europa','2'=>'América'))
                 ->field_type('idiomas','hidden');
            $crud->add_action('Traducir','',base_url('entradas/admin/distribuidores/traducir').'/');
            $crud = $crud->render();
            $crud->output = $this->load->view('distribuidores',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }

        function galeria(){
            $crud = $this->crud_function('','');            
            $crud->set_order('orden')            
                 ->set_clone();         
            $crud->field_type('foto','image',array('path'=>'img/galeria','width'=>'1024px','height'=>'758px'));
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function premios(){
            $crud = $this->crud_function('','');
            $crud->set_clone()         
                 ->field_type('foto','image',array('path'=>'img/vinos','width'=>'960px','height'=>'954px'))
                 ->field_type('logo_premio','image',array('path'=>'img/vinos','width'=>'120px','height'=>'120px'))
                 ->field_type('idiomas','hidden');
            $crud->add_action('Traducir','',base_url('entradas/admin/premios/traducir').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function fechas_bloqueadas(){
            $crud = $this->crud_function('','');            
            $crud->set_subject('Fecha');
            $crud = $crud->render();
            $crud->title = 'Bloqueo de fechas';
            $this->loadView($crud);
        }
        function reservas(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
