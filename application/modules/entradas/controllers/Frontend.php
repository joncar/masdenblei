<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        } 
        
        

        function vinos($enlace){
    		if(!empty($enlace)){
    			$enlace = str_replace('-',' ',$enlace);
    			$vino = $this->elements->vinos(array('enlace'=>$enlace));

    			if($vino->num_rows()>0){
    				$vino = $vino->row();
    				$recomendados = $this->elements->vinos(array('id !='=>$vino->id));
    				$page = $this->load->view($this->theme.'vins-detail',array('detail'=>$vino,'recomendados'=>$recomendados),TRUE,'paginas');
    				$this->loadView(
    					array('view'=>'read','page'=>$page,'title'=>$vino->nombre)
    				);
    			}
    		}
        }

        function reservas(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('reservas')
                 ->set_theme('bootstrap2')
                 ->set_subject('Reserva')
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_list()
                 ->unset_back_to_list()
                 ->required_fields_array();                 
            $crud = $crud->render();            
        }
    }
?>
