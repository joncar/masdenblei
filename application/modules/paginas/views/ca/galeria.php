<body class="blog body_tag body_style_wide scheme_default blog_mode_blog  is_stream blog_style_portfolio_3 sidebar_hide expand_content header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive galeria">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('Galeria') ?></h1>
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= l('Galeria') ?></span></div>
						</div>
					</div>
				</div>
		</header>
		<div class="menu_mobile_overlay"></div>
		<div class="menu_mobile scheme_dark">
			<div class="menu_mobile_inner">
				<a class="menu_mobile_close icon-cancel"></a>
				<nav class="menu_mobile_nav_area">
					<ul id="menu_mobile" class="menu_mobile_nav">
						<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
					</ul>
				</nav>
				<div class="search_mobile">
					<?= $this->load->view($this->theme.'_search') ?>
				</div>
				<div class="socials_mobile">
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
				</div>
			</div>
		</div>
		<div class="page_content_wrap scheme_default">
			

			
			<div class="wrap">
				<div class="content">
					<div class="portfolio_filters laon_wine_house_tabs">
						



						<div  class="portfolio_content laon_wine_house_tabs_content" data-blog-template="622" data-blog-style="portfolio_3" data-posts-per-page="9" data-cat="0" data-parent-cat="0" data-need-content="false">
							<div id="portfolio_filters_0_content" class="portfolio_wrap posts_container portfolio_4">
								<?php foreach($this->elements->galeria()->result() as $g): ?><div class="post_item post_layout_portfolio post_layout_portfolio_3 post-77 post type-post has-post-thumbnail" data-src="<?= $g->foto ?>">
										<div class="post_featured with_thumb hover_icons">
											<img width="370" height="274" src="<?= $g->foto ?>" alt="<?= l('Galeria de fotos') ?>" />
											<div class="mask"></div>
											<div class="icons">
												<div>
													<a href="<?= $g->foto ?>" aria-hidden="true" class="icon-search" title="<?= l('Galeria de fotos') ?>"></a>
												</div>
											</div>
										</div>
									</div><?php endforeach ?>
							</div>


						</div>




					</div>
				</div>
			</div>



			<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>	</div>
</div>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA"></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/js/embedded-map.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/vendor/jquery-resize/jquery.ba-resize.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/js/tribe-events.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>js/lightgallery/js/lightgallery.js'></script>
<script>
	    jQuery(document).ready(function() {
	        jQuery("#portfolio_filters_0_content").lightGallery(); 
	    });
</script>
<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>