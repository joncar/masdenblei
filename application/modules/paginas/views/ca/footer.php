<!-- FOOTER -->
<footer class="site_footer_wrap scheme_dark" id="footer">
    <div class="container">
        <div class="footer">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="f-block1">
                        <a href="<?= base_url() ?>">
                            <img class="img-responsive" src="[base_url]theme/theme/images/logo-footer.png" alt="">
                        </a>
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <span class="specific"><?= l('Adreça') ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <p> <?= l('direccion') ?></p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <span class="specific"><?= l('Telèfon') ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <p><a href="tel:+34686246679">+34 686 246 679</a></p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <span class="specific">fax</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <p>(415) 899-3457</p>
                                    </div>
                                </div>-->
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <span class="specific">email</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <a href="mailto:info@masdenblei.com">info@masdenblei.com</a>
                                    </div>
                                </div>
                            </li>                        
                    </ul>                    
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="f-block2">
                        <div class="links">
                            <h4 class="title">web links</h4>
                            <ul>
                            	<li style="margin:20px 0"><a href="https://www.doqpriorat.org/ "><img src="<?= base_url('img/logo-footer.svg') ?>" style="margin-top: -28px;"></a></li>
								<li><a href="<?= base_url() ?>"><?= l('INICI') ?></a></li>
								<li><a href="<?= base_url() ?>blog.html"><?= l('ACTUALITAT') ?></a></li>
								<li><a href="<?= base_url() ?>galeria.html"> <?= l('GALERIA') ?></a></li>								
                            </ul>
                        </div>
                        <div class="links">
                            <h4 class="title"><?= l('Notícies') ?></h4>
                            <ul>
                                <?php 
                                    $this->db->limit(2);
                                    $this->db->order_by('fecha','DESC');
                                    foreach($this->elements->blog()->result() as $b): ?>
                                    <li style="text-overflow: ellipsis;overflow: hidden; margin: 0 0 20px 0;">
                                        <a href="<?= $b->link ?>">
                                            <img src="<?= $b->foto ?>" style="width:60px; vertical-align: top; margin-right: 5px;">
                                            <div style="width:141px; display: inline-block;"><?= $b->titulo ?></div>
                                        </a>
                                    </li>
                                <?php endforeach ?>                                    
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="f-block3">
                        <form action="paginas/frontend/subscribir" onsubmit="sendForm(this,'.resultSubs'); return false;">
                            <div class="date">
                                <h4 class="title"><?= l('Informat') ?></h4>
                                <p><?= l('Subscriu-te al butlletí') ?></p>
                                <div class="resultSubs" style="display: inline-block;width: 100%;"></div>
                                <div class="input-group">
	                                <input type="email" placeholder="<?= l('Email de contacte'); ?>" class="form-control footer-nl" name="email">
	                                <span class="input-group-btn">
	                                    <button type="submit" class="btn btn-default"><i class="fa fa-envelope"></i></button>
	                                </span>
                                </div>
                                <div class="form-group">
	                                <p><input type="checkbox" name="politicas" value="1"> <a href="<?= base_url().'politica-de-privacitat.html' ?>" target="_new"><?= l('acepto-las-politicas-de-privacidad'); ?></a></p>
	                            </div>
                            </div>

                        </form>
                        <div class="social">
                            <h4 class="title"><?= l('xarxes socials') ?></h4>
                            <ul>                                
                                <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.pinterest.com/"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a></li>                                
                                <li><a href="http://www.youtube.com/"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="f-block4">
                        <h4 class="title"><?= l('CONTACTE') ?></h4>
                        <div class="form">
                            
                            <form id="footercontacto" onsubmit="return sendForm(this,'#mensajeFooter')" action="paginas/frontend/contacto" method="post">
                            	<div id="mensajeFooter"></div>
                                <div class="form-group">
                                    <input name="nombre" id="senderName" type="text" class="form-control" placeholder="<?= l('Nom i Cognoms'); ?>" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" id="senderEmail" class="form-control" placeholder="<?= l('Email de contacte'); ?> " required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="telefono" id="senderEmail" class="form-control" placeholder="<?= l('Telèfon de contacte'); ?> " required="required" />
                                </div>
                                <div class="form-group">
                                    <textarea name="extras[message]" id="message" class="form-control" rows="3" placeholder="<?= l('Missatge'); ?>"></textarea>
                                </div>
                                <div class="form-group">
                                    <p><input type="checkbox" name="politicas" value="1"> <a href="<?= base_url().'politica-de-privacitat.html' ?>" target="_new"><?= l('acepto-las-politicas-de-privacidad'); ?></a></p>
                                </div>
                                <button type="submit" class="sc_button_hover_slide_top"><?= l('Enviar missatge'); ?></button>
                            </form>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 cap col-centered center-block">
            
        </div>
        <div class="footer-bot">
            <ul>                
                <li><a href="<?= base_url() ?>politica-de-privacitat.html"><?= l('Política de privacitat') ?></a></li>
            </ul>
            <p><?= l('copyright') ?></p>
        </div>
    </div>
</footer>