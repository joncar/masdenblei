<body class="cellerPage page page-id-36 page-template-default body_tag body_style_wide scheme_default blog_mode_page is_single sidebar_hide expand_content remove_margins header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('Política de privacitat') ?></h1>

							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= l('Política de privacitat') ?></span></div>
						</div>
					</div>
				</div>
				<div class="post_featured post_featured_fullwide" style="background-image:url(<?= base_url() ?>theme/theme/images/image-14.jpg);"></div>
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search') ?>
					</div>
					<div class="socials_mobile">
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
					</div>
				</div>
			</div>
		</div>
	
	
	</div>
</div>
</div>


















<div class="wrap cellerEquipo">
<div class="row">
	
	<div class="wpb_column vc_column_container column-12_12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				<div class="vc_empty_space  height_medium" style="height: 0px"></div>
				<div class="wpb_wrapper">
					<div class="vc_empty_space  height_tiny" style="height: 0px"></div>
					<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
						<div class="sc_promo_text" style="width: 100%;">
							<div class="sc_promo_text_inner sc_align_center">
								<?= l('politica text') ?>
								
							</div>
						</div>
					</div>
					<div class="vc_empty_space  height_large hide_on_mobile" style="height: 0px"></div>
					
				</div>
				<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
				
			</div>
		</div>
	</div>
</div>
</div>
</div>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
</div>
</div>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/swiper/swiper.jquery.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/magnific/jquery.magnific-popup.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/trx_addons.js'></script>

<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>