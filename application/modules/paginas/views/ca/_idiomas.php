<div class="search_wrap search_style_fullscreen inited idiomasBar">
	<div class="search_form_wrap">
		<ul class="idiomasInterno" style="display: flex;">
            <li><a href="<?= base_url() ?>main/traduccion/es" style="font-weight:400; font-family: montserrat; <?= $_SESSION['lang']=='es'?'text-decoration: underline':''; ?>">ES</a></li>
            <li><a href="<?= base_url() ?>main/traduccion/ca" style="font-weight:400; font-family: montserrat; <?= $_SESSION['lang']=='ca'?'text-decoration: underline':''; ?>">CA</a></li>
            <li><a href="<?= base_url() ?>main/traduccion/en" style="font-weight:400; font-family: montserrat; <?= $_SESSION['lang']=='en'?'text-decoration: underline':''; ?>">EN</a></li>
      	</ul>
	</div>
</div>