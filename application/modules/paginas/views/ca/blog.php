<body class="blog body_tag body_style_fullscreen scheme_default blog_mode_blog  is_stream blog_style_chess_2 sidebar_hide expand_content remove_margins header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('Actualitat') ?></h1>
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= l('Actualitat') ?></span></div>
						</div>
					</div>
				</div>
		</header>
		<div class="menu_mobile_overlay"></div>
		<div class="menu_mobile scheme_dark">
			<div class="menu_mobile_inner">
				<a class="menu_mobile_close icon-cancel"></a>
				<nav class="menu_mobile_nav_area">
					<ul id="menu_mobile" class="menu_mobile_nav">
						<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
					</ul>
				</nav>
				<div class="search_mobile">
					<?= $this->load->view($this->theme.'_search',array(),TRUE,'paginas') ?>
				</div>
				<div class="socials_mobile">
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
				</div>
			</div>
		</div>
		<div class="blog page_content_wrap scheme_default">
			<div class="content">
				<div class="chess_wrap posts_container">
					

					<?php foreach($this->elements->blog()->result() as $b): ?>
						<article class="post_item post_layout_chess post_layout_chess_2 has-post-thumbnail" data-animation="animated fadeInUp normal">
							<div class="post_featured with_thumb hover_icon post_featured_bg" style="background-image: url('<?= $b->foto ?>');">
								<div class="mask"></div>
								<div class="icons">
									<a href="<?= $b->link ?>" aria-hidden="true" class="icon-glass"></a>
								</div>
							</div>
							<div class="post_inner">
								<div class="post_inner_content">
									<div class="post_header entry-header">
										<h3 class="post_title entry-title"><a href="<?= $b->link ?>" rel="bookmark"><?= $b->titulo ?></a></h3>
										<div class="post_meta" <span class="post_meta_item post_categories"><a href="<?= $b->link ?>" rel="category tag"></a><a href="<?= $b->link ?>" rel="category tag"></a></span>
											<span class="post_meta_item post_date"><a href="<?= $b->link ?>"><?= $b->fecha ?></a></span>											
										</div>
									</div>
									<div class="post_content entry-content">
										<div class="post_content_inner">
											<p><?= $b->texto ?></p>
										</div>
										<p><a class="more-link" href="<?= $b->link ?>"><?= l('Llegir més') ?></a></p>
									</div>
								</div>
							</div>
						</article>
					<?php endforeach ?>


				</div>
			</div>
		</div>
		<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
	</div>
	</div>
	<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
	<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>