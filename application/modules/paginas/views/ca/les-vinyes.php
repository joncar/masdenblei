<body class="remove_margins blog body_tag body_style_wide scheme_default blog_mode_blog  is_stream blog_style_portfolio_3 sidebar_hide expand_content header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('Les Vinyes') ?></h1>
							</div>
							<div class="breadcrumbs">
								<a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a>
								<span class="breadcrumbs_delimiter"></span>
								<span class="breadcrumbs_item current"><?= l('Les Vinyes') ?></span>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="post_featured post_featured_fullwide" style="background-image:url(<?= base_url() ?>theme/theme/images/d2.jpg);"></div> -->
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search') ?>
					</div>
					<div class="socials_mobile">
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
					</div>
				</div>
			</div>
			<div class="page_content_wrap scheme_default">
				<div class="wrap">
					<div class="row">
						<div class="wpb_column vc_column_container column-2_12">
							<div class="vc_column-inner ">
								<div class="wpb_wrapper"></div>
							</div>
						</div>
						<div class="wpb_column vc_column_container column-8_12">
							<div class="vc_column-inner ">
								<div class="wpb_wrapper">
									<div class="vc_empty_space  height_tiny" style="height: 0px"></div>
									<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
										<div class="sc_promo_text" style="width: 100%;">
											<div class="sc_promo_text_inner sc_align_center">
											
												<div class="sc_promo_icon" data-icon="icon-grape-3" style=" margin-bottom: 2px;">
													<img src="<?= base_url() ?>theme/theme/images/propi.png" alt=""  width="90px" height="276">
												</div>
												<h2 class="sc_item_title sc_promo_title"><?= l('Propietat') ?></h2>
												
												</div>												
											</div>
										</div>
									</div>
									<div class="vc_empty_space  height_large hide_on_mobile" style="height: 0px"></div>
									<div class="vc_empty_space  height_tiny" style="height: 0px"></div>
								</div>
							</div>
						</div>
						<div class="wpb_column vc_column_container column-2_12">
							<div class="vc_column-inner ">
								<div class="wpb_wrapper"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="page_content_wrap">
					<div class="row mapasvg-content">
						<div class="col-xs-12 col-md-3 text-center mapasvg-left">
							<ul class="mapaCeller">
								<li data-region="5">
									<?= l('vinyes1') ?>
								</li>
								<li data-region="6">
									<?= l('vinyes2') ?>									
								</li>
								<li data-region="7">
									<?= l('vinyes3') ?>									
								</li>
								<li data-region="8">
									<?= l('vinyes4') ?>
								</li>
							</ul>
						</div>
						<div class="col-xs-12 col-md-offset-2 col-md-8 mapasvg">
							<?php require_once('img/mapa2.svg') ?>
						</div>
						<div class="col-xs-12 col-md-3 text-center mapasvg-right">
							<ul class="mapaCeller">
								<li data-region="1">
									<?= l('vinyes5') ?>									
								</li>
								<li data-region="2">
									<?= l('vinyes6') ?>									
								</li>
								<li data-region="3">
									<?= l('vinyes7') ?>									
								</li>
								<li data-region="4">
									<?= l('vinyes8') ?>									
								</li>
							</ul>
						</div>
					</div>
				</div>
				











				



				
					
				
					
				

					
								






									

									



								






	

						





								

								

			</div>
			<div class="row vc_custom_1465549560104">
				<div class="wpb_column vc_column_container column-12_12">
					<div class="vc_column-inner ">
						<div class="wrap">
							<div class="vc_empty_space  height_huge" style="height: 0px"></div>
							<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
							<div class="wpb_text_column wpb_content_element ">
								<div class="wpb_wrapper">
									<h3 style="text-align: center;"><span style="color: #ffffff;"><?= l('vinyes9') ?></span></h3>
								</div>
							</div>
							<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
							<div class="vc_empty_space  height_huge" style="height: 0px"></div>
						</div>
					</div>
				</div>
			</div>
			<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
		</div>
	</div>

	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/jquery.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/jquery-migrate.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/theme.gallery/modernizr.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/theme.gallery/classie.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/swiper/swiper.jquery.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/theme.gallery/imagesloaded.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/masonry.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/theme.gallery/theme.gallery.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/magnific/jquery.magnific-popup.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/trx_addons.js'></script>

	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/ui/core.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/ui/widget.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/ui/tabs.min.js'></script>

	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.parallax.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.video.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/custom/rev_slider_1_1.js'></script>

		<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/__scripts.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/custom/custom.js'></script>
	<script>
		jQuery(".rosa, .mapaCeller li").on('mouseover',function(){
			/*jQuery(this).addClass('activado')*/
			var region = jQuery(this).data('region');
			jQuery('.mapaCeller li[data-region="'+region+'"]').addClass('active');
			jQuery('.rosa[data-region="'+region+'"]').addClass('activado');
		});
		jQuery(".rosa, .mapaCeller li").on('mouseout',function(){
			/*jQuery(this).removeClass('activado')*/
			var region = jQuery(this).data('region');
			jQuery('.mapaCeller li[data-region="'+region+'"]').removeClass('active');
			jQuery('.rosa[data-region="'+region+'"]').removeClass('activado');
		});
	</script>
	<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>