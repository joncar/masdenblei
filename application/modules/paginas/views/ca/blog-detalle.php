<body class="single single-post postid-194 single-format-standard body_tag body_style_wide scheme_default blog_mode_post is_single sidebar_hide header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive remove_margins blogDetalle">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno',array(),FALSE,'paginas'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<div class="post_meta"> 
									<span class="post_meta_item post_date date updated"><a href="#"><?= $detail->fecha ?></a></span>
								</div>
								<h1 class="page_caption"><?= $detail->titulo ?></h1>
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>">Inici</a><span class="breadcrumbs_delimiter"></span><a class="breadcrumbs_item cat_post" href="<?= base_url() ?>blog.html">Actualitat</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= $detail->titulo ?></span></div>
						</div>
					</div>
				</div>				
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search',array(),TRUE,'paginas') ?>
					</div>
					<div class="socials_mobile"><span class="social_item"><a href="javascript:void(0)" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a>
						</span><span class="social_item"><a href="javascript:void(0)" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a>
						</span><span class="social_item"><a href="javascript:void(0)" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a>
						</span>
					</div>
				</div>
			</div>
			





			<article class="post_item post_item_single post_layout_chess post_layout_chess_1 post has-post-thumbnail">
				<a id="sc_anchor_post_75" class="sc_anchor" title="<?= $detail->titulo ?>"></a>
				<div class="post_featured with_thumb hover_icon post_featured_bg" style="background-image: url('<?= $detail->foto ?>'); height:440px;"></div>
				<div class="post_inner">
					<div class="post_inner_content">
						<div class="post_content entry-content">
							<div class="">
								<h4 class="post_title entry-title">
									<a href="javascript:void(0)" rel="bookmark"><?= $detail->titulo ?></a>
								</h4>
								<?= $detail->texto ?>

								<div class="post_meta post_meta_single" style="margin-top:0">
									<span class="post_meta_item post_tags">
										<span class="post_meta_label">Tags:</span> 
										<?php foreach(explode(',',$detail->tags) as $t): ?>
											<a href="javascript:void(0)" rel="tag"><?= $t ?></a>, 
										<?php endforeach ?>										
									</span>
									<span class="post_meta_item post_share">
										<span class="socials_wrap socials_share socials_size_tiny socials_type_block socials_dir_horizontal">
											<span class="social_items inited">
												<span class="social_item social_item_popup">
													<a href="https://twitter.com/share?text=<?= urlencode($detail->titulo) ?>&url=<?= base_url('blog/'.toUrl($detail->id.'-'.$detail->titulo)) ?>" class="social_icons social_twitter sc_button_hover_slide_top" data-link="#" data-count="twitter">
														<span class="trx_addons_icon-twitter"></span>
													</a>
												</span>
												<span class="social_item social_item_popup">
													<a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('blog/'.toUrl($detail->id.'-'.$detail->titulo)) ?>" class="social_icons social_facebook sc_button_hover_slide_top" data-link="#" data-count="facebook">
														<span class="trx_addons_icon-facebook"></span>
													</a>
												</span>												
											</span>
										</span>
									</span>									
								</div>
							</div>
						</div>
						
					</div>
				</div>



			</article>			
			<article class="blogprevnext scheme_default" style="margin: 100px 0;">
				<div id="tribe-events">
					<ul class="tribe-events-sub-nav">
						<?php if(!empty($prev)): ?>
							<li class="tribe-events-nav-previous">
								<a href="<?= $prev ?>" rel="next" class="sc_button_hover_slide_top"> <span>»</span> Post Anterior </a>
							</li>
						<?php endif ?>
						<?php if(!empty($prev)): ?>
							<li class="tribe-events-nav-next">
								<a href="<?= $next ?>" rel="next" class="sc_button_hover_slide_top">Post Següent <span>»</span></a>
							</li>
						<?php endif ?>
					</ul>
				</div>
			</article>












	<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
	</div>
	</div>
	<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
	<a href="javascript:void(0)" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>