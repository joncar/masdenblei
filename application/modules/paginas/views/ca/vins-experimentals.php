<body class="remove_margins blog body_tag body_style_wide scheme_default blog_mode_blog  is_stream blog_style_portfolio_3 sidebar_hide expand_content header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption">Vins experimentals</h1>
							</div>
							<div class="breadcrumbs">
								<a class="breadcrumbs_item home" href="<?= base_url() ?>">Home</a>
								<span class="breadcrumbs_delimiter"></span>
								<span class="breadcrumbs_item current">Vins experimentals</span>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="post_featured post_featured_fullwide" style="background-image:url(<?= base_url() ?>theme/theme/images/d2.jpg);"></div> -->
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search') ?>
					</div>
					<div class="socials_mobile">
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a>
						</span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a>
						</span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a>
						</span>
					</div>
				</div>
			</div>
				




					<!-- 
<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>					
 -->







			
					<article id="post-396" class="post_item_single post_type_page post-396 page type-page">

							<div class="wrap">
								<div class="row">
									<div class="wpb_column vc_column_container column-2_12">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper"></div>
										</div>
									</div>
									<div class="wpb_column vc_column_container column-8_12">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
													<div class="sc_promo_text" style="width: 100%;">
														<div class="sc_promo_text_inner sc_align_center">
												<div class="sc_promo_icon" data-icon="icon-grape-3" style=" margin-bottom: 2px;"><img src="http://hipo.tv/masdenblei/theme/theme/images/ex.png" alt=""  width="90px" height="276"></span></div>															
															<h5 class="sc_item_subtitle sc_promo_subtitle">
															<?= l('vins') ?>
															</h5>	

																											
														</div>
													</div>
												</div>												
											</div>
										</div>
									</div>									
								</div>
							</div>

				
					<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
					<article class="wpb_row post_item post_layout_chess post_layout_chess_1 post has-post-thumbnail" id="post_75">
						<a id="sc_anchor_post_75" class="sc_anchor" title="How to Pair Different Wines with Your Favorite Food" data-icon="" data-url=""></a>
						<div class="post_featured with_thumb hover_icon trx-stretch-height post_featured_bg" style="background-image: url(<?= base_url() ?>theme/theme/images/image-6.jpg); height: 949px; float:left;"></div>
						<div class="post_inner" style="right:0; left:auto;">
							<div class="post_inner_content">
							
								
									

									
									
									<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
										<div class="sc_promo_text" style="width: 100%;">
											<div class="sc_promo_text_inner sc_align_center">
												<div class="sc_promo_icon" data-icon="icon-grape-3" style=" margin-bottom: 2px;"><img src="http://hipo.tv/masdenblei/theme/theme/images/celler.png" alt=""  width="90px" height="276"></span></div>
												
												<h5 class="sc_item_subtitle sc_promo_subtitle"><?= l('esfe') ?></h5>
												<div class="sc_promo_content sc_item_content">
													<div class="wpb_text_column wpb_content_element ">
														<div class="wpb_wrapper">
															<?= l('esfe2') ?>
															</div>
													</div>
												</div>												
											</div>
										</div>
									</div>
									
									<div class="post_meta"> 
										<span class="post_meta_item post_categories"></span>
									</div>	
									
									
									
									
								
								
							</div>
						</div>
											
					</article>
				

					
								






									

									



								






	

						





								

								

			</div>
			<div class="row vc_custom_1465549560104">
									<div class="wpb_column vc_column_container column-12_12">
										<div class="vc_column-inner ">
											<div class="wrap">
												<div class="vc_empty_space  height_huge" style="height: 0px"></div>
												<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<h3 style="text-align: center;"><span style="color: #ffffff;"><?= l('distribuidors1'); ?></span></h3>
													</div>
												</div>
												<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
												<div class="vc_empty_space  height_huge" style="height: 0px"></div>
											</div>
										</div>
									</div>
								</div>
			<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
		</div>
	</div>

	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/jquery.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/jquery-migrate.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/theme.gallery/modernizr.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/theme.gallery/classie.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/swiper/swiper.jquery.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/theme.gallery/imagesloaded.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/masonry.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/theme.gallery/theme.gallery.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/magnific/jquery.magnific-popup.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/trx_addons.js'></script>

	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/ui/core.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/ui/widget.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/ui/tabs.min.js'></script>

	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.parallax.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.video.min.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/custom/rev_slider_1_1.js'></script>

		<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/__scripts.js'></script>
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/custom/custom.js'></script>
	<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>