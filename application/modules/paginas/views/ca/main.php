<body class="page page-id-584 page-template-default body_tag scheme_default blog_mode_page is_single sidebar_hide expand_content remove_margins header_style_header-2 header_title_off menu_style_side no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_main top_panel_style_2 with_bg_image with_bg_video header_fullheight trx-stretch-height scheme_default" data-video="<?= base_url('img/'.$this->db->get('ajustes')->row()->banner) ?>" style="background-image: url(<?= base_url('img/'.$this->db->get('ajustes')->row()->banner_img) ?>)">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<div class="top_panel_navi with_bg_image scheme_dark">
					<div class="menu_main_wrap clearfix">
						<div class="wrap">
							<a class="logo" href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/theme/images/logo-footer.png" class="logo_main" alt="" /></a>
						</div>
					</div>
				</div>
				<div class="top_panel_title_2_wrap">
					<div class="content_wrap">
						<div class="top_panel_title_2">
							<div class="top_panel_title_2_image caveat">
								<?= l('Vinyes amb aires mediterranis'); ?>
							</div>
							<div class="top_panel_title_2_text">
								<?= l('En aquest racó privilegiat del Priorat, mantenim viu un llegat històric del qual hi ha constància des del 1756'); ?>								
							</div>
						</div>
					</div>
				</div>
				<div class="top_panel_navi_header with_bg_image scheme_default">
					<div class="menu_header_wrap clearfix">
						<div class="content_wrap">
							<nav class="menu_header_nav_area menu_hover_fade">
								<ul id="menu_header" class="menu_header_nav">
									<li id="menu-item-485" class="menu-item menu-item-485"><a href="<?= base_url() ?>el-celler.html"><span><?= l('QUI SOM?') ?></span></a></li>
									<li id="menu-item-486" class="menu-item menu-item-486"><a href="<?= base_url() ?>els-vins.html"><span><?= l('ELS NOSTRES VINS') ?></span></a></li>
									<li id="menu-item-484" class="menu-item menu-item-484"><a href="<?= base_url() ?>blog"><span><?= l('ACTUALITAT'); ?></span></a></li>
									<li id="menu-item-484" class="menu-item menu-item-484"><a href="<?= base_url() ?>distribuidors.html"><span><?= l('DISTRIBUÏDORS'); ?></span></a></li>									
								</ul>
							</nav>
						</div>
					</div>
					<div class="mouseContent"><p class="mouse"></p></div>
				</div>

			</header>
			<div class="menu_side_wrap scheme_dark">
				<div class="menu_side_inner">
					<a class="menu_mobile_button menu_mobile_button_text"><?= l('MENÚ'); ?></a>
					<ul class="idiomas">
		                <li><a href="<?= base_url('main/traduccion/es') ?>" class="menu_mobile_button_text" style="font-family: montserrat;font-weight:400; <?= $_SESSION['lang']=='es'?'text-decoration:underline':''; ?>">ES</a></li>
		                <li><a href="<?= base_url('main/traduccion/ca') ?>" class="menu_mobile_button_text" style="font-family: montserrat;font-weight:400; <?= $_SESSION['lang']=='ca'?'text-decoration:underline':''; ?>">CA</a></li>
		                <li><a href="<?= base_url('main/traduccion/en') ?>" class="menu_mobile_button_text" style="font-family: montserrat;font-weight:400; <?= $_SESSION['lang']=='en'?'text-decoration:underline':''; ?>">EN</a></li>
	              	</ul>
					<ul class="menuLateralRedes">
						<li><a href="https://www.facebook.com/Mas-den-Blei-DOQ-Priorat-553931508060992/ "> <span class="trx_addons_icon-facebook"></span> </a></li>
						<li><a href="https://www.instagram.com/masdenblei/ "> <span class="trx_addons_icon-instagram"></span> </a></li>						
					</ul>
				</div>
			</div>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search',array(),TRUE,'paginas') ?>
					</div>
					<div class="socials_mobile">
						<span class="social_item"><a href="https://www.facebook.com/Mas-den-Blei-DOQ-Priorat-553931508060992/ "> <span class="trx_addons_icon-facebook"></span> </a></span>
						<span class="social_item"><a href="https://www.instagram.com/masdenblei/ "> <span class="trx_addons_icon-instagram"></span> </a></span>						
					</div>
				</div>
			</div>

			<div class="page_content_wrap scheme_default">
				<div class="content_wrap">
					<div class="content">
						<article id="post-584" class="post_item_single post_type_page post-584 page type-page has-post-thumbnail">
							<div class="post_content entry-content">
							<div class="row row-no-padding vc_custom_1466087475749 row-o-full-height row-o-columns-stretch row-o-equal-height">
								<div class="wpb_column vc_column_container column-12_12">
									<div class="vc_column-inner ">
										<div class="wpb_wrapper">
											<a id="sc_anchor_favourite_place" class="sc_anchor" title="RACÓ PRIVILEGIAT" data-icon="" data-url=""></a>
											<div class="sc_promo sc_promo_default sc_promo_size_large">
												
												<div class="sc_promo_text trx_addons_stretch_height" style="width: 50%;float: left;">
													<div class="sc_promo_text_inner sc_align_center">
														<div class="sc_promo_icon" data-icon="icon-grape-3">
															<span class="icon-grape-3"></span>
														</div>
														<h2 class="sc_item_title sc_promo_title"><?= l('RACÓ PRIVILEGIAT') ?></h2>														
														<div class="sc_promo_content sc_item_content">
															<div class="wpb_text_column wpb_content_element ">
																<div class="wpb_wrapper">
																	<?= l('home_about') ?>																	
																</div>
															</div>
														</div>
														<div class="sc_promo_content sc_item_content">
															<div class="wpb_single_image wpb_content_element vc_align_center" style=" margin-top: 5%;">
																<figure class="wpb_wrapper vc_figure">
																	<div class="vc_single_image-wrapper vc_box_border_grey">
																		<img width="90%" height="105" src="<?= base_url() ?>theme/theme/images/sinature.png" class="vc_single_image-img attachment-full" alt="sinature" />
																	</div>
																</figure>
															</div>
														</div>
														<div class="sc_item_button sc_promo_button sc_button_wrap">
															<a href="<?= base_url() ?>el-celler.html" class="sc_button sc_button_simple sc_button_size_normal sc_button_icon_top"><span class="sc_button_icon"><span class="icon-down-big"></span></span><span class="sc_button_text"><span class="sc_button_title"><?= l('QUI SOM') ?></span></span> </a>
														</div>
													</div>
												</div>

												<div class="sc_promo_image" style="background-image:url(<?= base_url() ?>theme/theme/images/image-21.jpg);width:50%;right: 0;">
													
													<div class="mainSect1">
														<h5 class="sc_item_subtitle sc_promo_subtitle">
															<?= l('home_vinyes'); ?>															
														</h5>
														
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						
							<div class="row row-no-padding row-o-full-height row-o-columns-middle flexreverse">
								<div class="wpb_column vc_column_container column-6_12">
									<div class="vc_column-inner ">
										<div class="wpb_wrapper">
											<div id="sc_content_142867621" class="sc_content sc_content_default home_products ">
												<div class="sc_content_container">
													<div class="woocommerce columns-2">
														<ul class="products">
															
															<?php foreach($this->elements->vinos(array('mostrar_en_main'=>1))->result() as $v): ?>
																<li class="post-403 product type-product has-post-thumbnail product_cat-red-wines product_tag-55 first instock shipping-taxable purchasable product-type-simple">
																		<div class="post_item post_layout_thumbs">
																			<div class="post_featured hover_shop">
																				<a href="<?= $v->link ?>">
																				<img width="360" height="480" src="<?= $v->foto ?>" alt="<?= $v->nombre ?>" title="<?= $v->nombre ?>" /> </a>
																				<div class="mask"></div>
																				<div class="icons">	
																					<div>
																						<a href="<?= $v->link ?>" aria-hidden="true" class="icon-eye-light"></a>
																					</div>
																				</div>
																			</div>
																			<div class="post_data">
																				<div class="post_header entry-header">
																					<div class="post_tags product_tags">
																						<a href="<?= $v->link ?>" rel="tag"><?= $v->anyo ?></a>
																					</div>
																					<h3>
																						<a href="<?= $v->link ?>"><?= $v->nombre ?></a>
																					</h3>
																				</div>																				
																			</div>
																		</div>
																</li>
															<?php endforeach ?>
														
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div><div class="wpb_column vc_column_container column-6_12">
							<div class="vc_column-inner ">
								<div class="wpb_wrapper">
									<a id="sc_anchor_our_wines" class="sc_anchor" title="<?= l('anchor ELS NOSTRES VINS') ?>" data-icon="" data-url=""></a>
									<div class="sc_promo sc_promo_default sc_promo_size_large sc_promo_no_image">
										<div class="sc_promo_text" style="width: 100%;">
											<div class="sc_promo_text_inner sc_align_center">
												<div class="sc_promo_icon" data-icon="icon-bottles"><span class="icon-bottles"></span></div>
												<?= l('home_nostres_vins') ?>
												<div class="sc_item_button sc_promo_button sc_button_wrap">
													<a href="<?= base_url() ?>els-vins.html" class="sc_button sc_button_simple sc_button_size_normal sc_button_icon_top">
														<span class="sc_button_icon"><span class="icon-down-big"></span></span>
														<span class="sc_button_text"><span class="sc_button_title"><?= l('LLISTA DE VINS') ?></span></span> 
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							</div>
							</div>







							<div class="row">
								<div class="vc_empty_space  height_large hide_on_mobile" style="height: 0px"></div>
								<div class="wpb_column vc_column_container column-2_12">
									<div class="vc_column-inner ">
										<div class="wpb_wrapper"></div>
									</div>
								</div>
								<div class="wpb_column vc_column_container column-8_12">
									<div class="vc_column-inner ">
										<div class="wpb_wrapper">
											<div class="vc_empty_space  height_tiny" style="height: 0px"></div>
											<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
												<div class="sc_promo_text" style="width: 100%;">
													<div class="sc_promo_text_inner sc_align_center">
														<div class="sc_promo_icon celler2" data-icon="icon-grape-3"><img src="<?= base_url() ?>theme/theme/images/actu.jpg" alt="" width="90px" height="276"></div>
														<h2 class="sc_item_title sc_promo_title"><?= l('ACTUALITAT') ?></h2>
														<h5 class="sc_item_subtitle sc_promo_subtitle"><?= l('Tot allò que fem, tot allò que som i tot allò que et volem explicar') ?></h5>														
													</div>
												</div>
											</div>
											<div class="vc_empty_space  height_large hide_on_mobile" style="height: 0px"></div>											
										</div>
									</div>
								</div>
								<div class="wpb_column vc_column_container column-2_12">
									<div class="vc_column-inner ">
										<div class="wpb_wrapper"></div>
									</div>
								</div>
							</div>
							<!----- BLOG ------->
								<div class="blogMain page_content_wrap scheme_default">
										<div class="content">
											<div class="chess_wrap posts_container">
												<?php foreach($this->elements->blog()->result() as $b): ?>
													<article id="post-77" class="post_item post_layout_chess post_layout_chess_3 post-77 post type-post has-post-thumbnail" data-animation="animated fadeInUp normal">
														<div class="post_featured with_thumb hover_icon post_featured_bg" style="background-image: url('<?= $b->foto ?>');">
															<div class="mask"></div>
															<div class="icons">
																<a href="<?= $b->link ?>" aria-hidden="true" class="icon-glass"></a>
															</div>
														</div>
														<div class="post_inner">
															<div class="post_inner_content">
																<div class="post_header entry-header">
																	<h3 class="post_title entry-title"><a href="<?= $b->link ?>" rel="bookmark"><?= $b->titulo ?></a></h3>
																	<div class="post_meta"><span class="post_meta_item post_categories">
																		<a href="<?= $b->link ?>" rel="category tag"></a>
																		<a href="<?= $b->link ?>" rel="category tag"></a></span>
																		<span class="post_meta_item post_date">
																			<a href="<?= $b->link ?>"><?= $b->fecha ?></a>
																		</span>
																	</div>
																</div>
																<div class="post_content entry-content">
																	<div class="post_content_inner">
																		<p><?= $b->texto ?></p>
																	</div>
																	<p><a class="more-link" href="<?= $b->link ?>"><?= l('Llegir més') ?></a></p>
																</div>
															</div>
														</div>
													</article>
												<?php endforeach ?>
												
											</div>
										</div>
									</div>
								<div class="vc_empty_space  height_large" style="height: 0px"></div>											
								<div class="sc_item_button sc_promo_button sc_button_wrap" style="text-align:center">
									<a href="http://hipo.tv/masdenblei/els-vins.html" class="sc_button sc_button_simple sc_button_size_normal sc_button_icon_top">
										<span class="sc_button_icon">
											<span class="icon-down-big"></span>
										</span>
										<span class="sc_button_text">
											<span class="sc_button_title"><?= l('VEURE MËS') ?></span>
										</span> 
									</a>
								</div>
								<div class="vc_empty_space  height_large" style="height: 0px"></div>											
								
								<div class="row row-no-padding row-o-full-height row-o-columns-middle">
									<div class="wpb_column vc_column_container column-12_12" style="background:#f7f7f7">
										<div class="vc_column-inner ">
											<div class="wrap premisContent">
												<a id="sc_anchor_awards" class="sc_anchor" title="<?= l('PREMIS') ?>"></a>												
												<div class="sc_icons sc_icons_default sc_icons_size_medium sc_align_center">
													<div class="sc_icons_columns sc_item_columns trx_addons_columns_wrap columns_padding_bottom">
														<div class="trx_addons_column-1_3">
															<div class="sc_icons_item sc_icons_item_linked">
																<div class="sc_icons_image"><img src="<?= base_url() ?>theme/theme/images/icon-01.png" alt="" width="276" height="276"></div>
																<h4 class="sc_icons_title"><span><?= l('PREMIS') ?></span></h4>
																<div class="sc_icons_description"><span><?= l('Els nostres vins finalistes en diferents concursos.') ?></span></div>
																<a href="<?= base_url('premis.html') ?>" class="sc_icons_link"></a>
															</div>
														</div><div class="trx_addons_column-1_3">
															<div class="sc_icons_item sc_icons_item_linked">
																<div class="sc_icons_image"><img src="<?= base_url() ?>theme/theme/images/icon-02.png" alt="" width="276" height="276"></div>
																<h4 class="sc_icons_title"><span><?= l('GALERIA') ?></span></h4>
																<div class="sc_icons_description"><span><?= l('Descobreix Mas den Blei en imatges.') ?></span></div>
																<a href="<?= base_url() ?>galeria.html#galeria" class="sc_icons_link"></a>
															</div>
														</div><div class="trx_addons_column-1_3">
															<div class="sc_icons_item sc_icons_item_linked">
																<div class="sc_icons_image"><img src="<?= base_url() ?>theme/theme/images/icon-03.png" alt="" width="276" height="276"></div>
																<h4 class="sc_icons_title"><span><?= l('distribuidores') ?></span></h4>
																<div class="sc_icons_description"><span><?= l('Troba els nostres vins arreu del món.') ?></span></div>
																<a href="<?= base_url() ?>distribuidors.html" class="sc_icons_link"></a>
															</div>
														</div>
													</div>
												</div>												
											</div>
										</div>
									</div>
								</div>




								<div class="scheme_dark wpb_row" style=" background-image: url(<?= base_url() ?>theme/theme/images/image-222.jpg);background-position: center;background-size: cover;">
									<div class="wrap" style="margin-bottom: 0">
										<div class="row">

											<div class="wpb_column vc_column_container column-2_12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper"></div>
												</div>
											</div>
											<div class="wpb_column vc_column_container column-8_12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="vc_empty_space  height_large hide_on_mobile" style="height: 0px"></div>
														<div class="vc_empty_space  height_medium" style="height: 0px"></div>
														<div class="sc_icons sc_icons_default sc_icons_size_large sc_align_center">
															<div class="sc_icons_item">
																<h2 class="sc_item_title sc_promo_title"><?= l('què opinen dels nostres vins') ?></h2>
												<div class="sc_promo_icon" data-icon="icon-bottles"><img src="<?= base_url() ?>theme/theme/images/rec.png"  style=" margin-top: 20px;" alt="" width="110px" height="276"></div>
															</div>
														</div>
														<div class="vc_empty_space  height_small" style="height: 0px"></div>
														<div class="sc_testimonials sc_testimonials_simple swiper-slider-container slider_swiper slider_no_controls" data-slides-min-width="150">
															<div class="sc_testimonials_slider sc_item_slider slides swiper-wrapper">
																<div class="swiper-slide">
																	<div class="sc_testimonials_item">
																		<div class="sc_testimonials_item_content">
																			<p>&#8220;<?= l('testimonio1') ?>;&#8221;</p>
																		</div>
																		<div class="sc_testimonials_item_author">
																			<div class="sc_testimonials_item_author_data">
																				<h4 class="sc_testimonials_item_author_title">Andreas Larsson, Millor Sommelier del Món 2007</h4>
																				<div class="sc_testimonials_item_author_subtitle"></div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="swiper-slide">
																	<div class="sc_testimonials_item">
																		<div class="sc_testimonials_item_content">
																			<p>&#8220;<?= l('testimonio2') ?>&#8221;</p>
																		</div>
																		<div class="sc_testimonials_item_author">
																			<div class="sc_testimonials_item_author_data">
																				<h4 class="sc_testimonials_item_author_title">Andreas Larsson, Millor Sommelier del Món 2007</h4>
																				<div class="sc_testimonials_item_author_subtitle"></div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="wpb_column vc_column_container column-2_12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper"></div>
												</div>
											</div>
										</div>
									</div>
								</div>


								<div class="row row-no-padding vc_custom_1466154433105 row-o-full-height row-o-columns-stretch row-o-equal-height">
									<div class="wpb_column vc_column_container column-4_12">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper"></div>
										</div>
									</div>
									<div class="wpb_column vc_column_container column-4_12 vc_col-has-fill">
										<div class="vc_column-inner vc_custom_1466756857898">
											<div class="wpb_wrapper">
												<a id="sc_anchor_address" class="sc_anchor" title="<?= l('Contacte') ?>" data-icon="" data-url=""></a>
												<div class="vc_empty_space  height_medium" style="height: 0px"></div>
												<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
													<div class="sc_promo_text sc_float_center trx_addons_stretch_height" style="width: 100%;">
														<div class="sc_promo_text_inner sc_align_center">
															<div class="sc_promo_icon" data-icon="icon-winery"><span class="icon-winery"></span></div>
															<h2 class="sc_item_title sc_promo_title"><?= l('CONTACTE') ?></h2>
															<h5 class="sc_item_subtitle sc_promo_subtitle"></h5>
															<div class="sc_promo_content sc_item_content">
																<div class="wpb_text_column wpb_content_element ">
																	<div class="wpb_wrapper">
																		<p><em><strong><?= l('Adreça') ?>:</strong></em><br/> <?= l('direccion') ?></p>
																		<p><em><strong><?= l('Telèfon') ?>:</strong></em><br/> +34 686 246 679</p>
																		<p><em><strong>E-mail:</strong></em><br/>  info@masdenblei.com</p>																		
																	</div>
																</div>
															</div>
															<div style="text-align: left !important;">
																<form id="contactoFooter" class="sc_form_form sc_input_hover_underline inited" method="post" action="paginas/frontend/contacto" style="display: none; margin-top: 30px;" onsubmit="sendForm(this,'',function(data){console.log(data);jQuery('.sc_form_result').html(data).show(); jQuery('body').on('click',function(){jQuery('.sc_form_result').hide();});}); return false;">
																	<label class="sc_form_field sc_form_field_name">
																		<span class="sc_form_field_wrap" style="text-align:left !important;"><input type="text" name="nombre"><span class="sc_form_field_hover" style="text-align:left !important;"><span class="sc_form_field_content" data-content="Name" style="text-align:left !important;"><?= l('Nom i Cognoms'); ?></span></span></span>
																	</label>
																	<label class="sc_form_field sc_form_field_name">
																		<span class="sc_form_field_wrap" style="text-align:left !important;"><input type="text" name="email"><span class="sc_form_field_hover" style="text-align:left !important;"><span class="sc_form_field_content" data-content="Email" style="text-align:left !important;"><?= l('Email de contacte'); ?></span></span></span>
																	</label>
																	<label class="sc_form_field sc_form_field_name">
																		<span class="sc_form_field_wrap" style="text-align:left !important;"><input type="text" name="telefono"><span class="sc_form_field_hover" style="text-align:left !important;"><span class="sc_form_field_content" data-content="Phone" style="text-align:left !important;"><?= l('Telèfon de contacte'); ?></span></span></span>
																	</label>
																	<label class="sc_form_field sc_form_field_message">
																		<span class="sc_form_field_wrap" style="text-align:left !important;"><textarea name="extras[message]"></textarea><span class="sc_form_field_hover" style="text-align:left !important;"><span class="sc_form_field_content" data-content="Message" style="text-align:left !important;"><?= l('Missatge'); ?></span></span></span>
																	</label>
																	<div class="sc_form_field">
																		<input type="checkbox" name="politicas" id="politicas" value="1">
																		<label class="sc_form_field sc_form_field_name" for="politicas"  style="text-align:left !important">
																			<a href="<?= base_url().'politica-de-privacitat.html' ?>" target="_new"><?= l('acepto-las-politicas-de-privacidad'); ?></a>
																		</label>
																	</div>
																	
																	<div class="sc_form_field sc_form_field_button">
																		<button class="sc_button_hover_slide_top" type="submit"><?= l('Enviar missatge'); ?></button>
																	</div>
																	<div class="trx_addons_message_box sc_form_result"></div>
																</form>
															</div>
															<div class="sc_item_button sc_promo_button sc_button_wrap" style=" margin-top: 30px;">
																<a href="javascript:void(0)" onclick="jQuery('#contactoFooter').toggle(); $(this).find('.sc_button_text').remove();" class="sc_button sc_button_simple sc_button_size_normal sc_button_icon_top">
																	<span class="sc_button_icon">
																		<span class="icon-down-big"></span>
																	</span>
																	<span class="sc_button_text">
																		<span class="sc_button_title"><?= l('Omple el formulari') ?></span>
																	</span> 
																</a>
															</div>
														</div>
													</div>
												</div>
												<div class="vc_empty_space  height_medium" style="height: 0px"></div>
											</div>
										</div>
									</div>
									<div class="wpb_column vc_column_container column-4_12">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper"></div>
										</div>
									</div>
								</div>			
							</div>
						</article>
					</div>
				</div>
			</div>
			<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
		</div>
	</div>
	<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>	
	<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/swiper/swiper.jquery.min.js'></script>
	<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>