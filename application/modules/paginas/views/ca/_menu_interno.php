<div class="top_panel_navi scheme_default">
		<div class="menu_main_wrap clearfix">
			<div class="wrap" style="position: relative;">
				<div class="search_wrap search_style_fullscreen">
					<?= $this->load->view($this->theme.'_search',array(),TRUE,'paginas') ?>
					<?= $this->load->view($this->theme.'_idiomas',array(),TRUE,'paginas') ?>
				</div>

				<a class="logo" href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/theme/images/logo-footer2.png" class="logo_main" alt="" /></a>
				<nav class="menu_main_nav_area menu_hover_fade">
					<ul id="menu_main" class="menu_main_nav">
						<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
					</ul>								
				</nav>
			</div>
		</div>
	</div>