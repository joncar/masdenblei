<body class="woocommerce woocommerce-pagesingle header_style_header-1 header_position_default header_title_on menu_style_top vc_responsive desktop_layout expand_content top_panel_fixed premis">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption">Premis</h1>
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>">Inici</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">Premis</span></div>
						</div>
					</div>
				</div>
				<div class="post_featured post_featured_fullwide" style="background-image:url(<?= base_url() ?>theme/theme/images/premis.jpg);"></div>
		</header>
		<div class="menu_mobile_overlay"></div>
		<div class="menu_mobile scheme_dark">
			<div class="menu_mobile_inner">
				<a class="menu_mobile_close icon-cancel"></a>
				<nav class="menu_mobile_nav_area">
					<ul id="menu_mobile" class="menu_mobile_nav">
						<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
					</ul>
				</nav>
				<div class="search_mobile">
					<?= $this->load->view($this->theme.'_search') ?>
				</div>
				<div class="socials_mobile"><span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a>
					</span><span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a>
					</span><span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a>
					</span>
				</div>
			</div>
		</div>
		<div class="page_content_wrap scheme_default">
			

			
			<div class="wrap premis">
					<div class="content">
					<div class="related products">					
					
					<ul class="products">

						<?php foreach($this->elements->premios()->result() as $p): ?>
						<li class="product scheme_default type-product has-post-thumbnail column-1_3">
							<div class="post_layout_chess_1 post_item post_layout_thumbs">
								<div class="post_featured hover_shop">
									<div class="logopremio">
										<img src="<?= $p->logo_premio ?>" alt="">
									</div>
									<!-- <img src="<?= $p->foto ?>" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="<?= $p->nombre ?>" title="<?= $p->nombre ?> " width="360" height="480"> -->
									<div class="text-center">
										<div class="post_header entry-header">
											<h1 class="page_caption">
												<div class="post_meta post_meta_sin_raya">
													
													<span class="post_meta_item post_date">
														<a href="#"><?= $p->anyo ?></a>
													</span>
												</div>
												<?= $p->nombre ?>
											</h1>											
											
										</div>
										
										<div class="post_content entry-content">											
											<div class="post_meta"> <span class="post_meta_item post_categories"></span></div>											
											<h4 class="post_title entry-title">
												<a href="javascript:void(0)" rel="bookmark"></a>
											</h4>
											<p><?= $p->concurso ?></p>
											
											<h4 class="post_title entry-title">
												<!-- <a href="javascript:void(0)" rel="bookmark">Premi</a> -->
											</h4>
											<p style=" color: #e39a30;"><?= $p->premio ?></p>
										</div>
										
									</div>
								</div>
										
							</div>
						</li>
						<?php endforeach ?>
						

					</ul>
				</div>		
				</div>
			</div>



			<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>	</div>
</div>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA"></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/js/embedded-map.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/vendor/jquery-resize/jquery.ba-resize.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/js/tribe-events.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>js/lightgallery/js/lightgallery.js'></script>
<script>
	    jQuery(document).ready(function() {
	        jQuery("#portfolio_filters_0_content").lightGallery(); 
	    });
</script>
<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>