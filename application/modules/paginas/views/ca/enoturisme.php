<body class="mobile_layout single single-tribe_events postid-371 tribe-filter-live body_tag body_style_wide scheme_default blog_mode_events  is_stream blog_style_excerpt sidebar_hide expand_content header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive events-single tribe-events-style-full tribe-events-style-theme tribe-theme-laon-wine-house page-template-page-php singular">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<div class="post_meta"></div>
								<h1 class="page_caption"><?= l('Visita el celler Mas den Blei') ?></h1>
							</div>
							<div class="breadcrumbs">
								<a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a>
								<span class="breadcrumbs_delimiter"></span>
								<a href="#"><?= l('Enoturisme') ?></a>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search') ?>
					</div>
					<div class="socials_mobile"><span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a>
					</span><span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a>
					</span><span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a>
				</span>
			</div>
		</div>
	</div>
	<div class="page_content_wrap scheme_default">
		
		
		<div class="wrap">
			<div class="content">
				<article id="post-0" class="post_item_single post_type_page post-0 page type-page status-draft">
					<div class="post_content entry-content">
						<div id="tribe-events" class="tribe-no-js" data-live_ajax="1" data-datepicker_format="" data-category="">
							<div class="tribe-events-before-html"></div><span class="tribe-events-ajax-loading"><img class="tribe-events-spinner-medium" src="js/vendor/plugins/the-events-calendar/src/resources/images/tribe-loading.gif" alt="Loading Events"/></span>
							<div id="tribe-events-content" class="tribe-events-single">
								<p class="tribe-events-back">
									<a href="#"> &laquo; All Events</a>
								</p>
								<h1 class="tribe-events-single-event-title"><?= l('Visita el celler Mas den Blei') ?></h1>
								<div class="tribe-events-schedule tribe-clearfix">
									<h2><span class="tribe-event-date-start"><?= l('Descobreix la nostra botega i lessència dels nostres vins') ?></span><span class="tribe-event-date-end"></span></h2>
								</div>
								
								
							</div>
						</div>
						<div class="tribe-events-after-html"></div>
					</div>
				</article>
			</div></div>
			
			
			<div class="wrap">
				<div class="content">
					<article id="post-0" class="post_item_single post_type_page post-0 page type-page status-draft">
						<div class="post_content entry-content">
							<div id="tribe-events" class="tribe-no-js" data-live_ajax="1" data-datepicker_format="" data-category="">
								<div class="tribe-events-before-html"></div><span class="tribe-events-ajax-loading"><img class="tribe-events-spinner-medium" src="js/vendor/plugins/the-events-calendar/src/resources/images/tribe-loading.gif" alt="Loading Events"/></span>
								<div id="tribe-events-content" class="tribe-events-single">
									<p class="tribe-events-back">
										<a href="#"> &laquo; All Events</a>
									</p>
									
									<div class="tribe-events-schedule tribe-clearfix">
										
										<div id="post-371" class="post-371 tribe_events type-tribe_events has-post-thumbnail tag-festival tribe_events_cat-festivals cat_festivals">
											
											<!--
											<div class="tribe-events-cal-links">
													<a class="tribe-events-gcal tribe-events-button" href="#" title="Add to Google Calendar">Calendari</a><a class="tribe-events-ical tribe-events-button" href="#" title="Download .ics file">+ iCal Export</a></div>
												-->
												<div class="tribe-events-single-section tribe-events-event-meta primary tribe-clearfix">
													<div class="row">
														<div class="col-xs-12 col-md-3">
															<h3 class="tribe-events-single-section-title"> <?= l('Informació sobre la visita') ?> </h3><br><br>
															<dl>
																<dt> <?= l('Horaris') ?>: </dt>
																<dd>
																<a href="" rel="tag"><?= l('horraris') ?> </a>
																</dd>
																<dt> <?= l('Grups') ?>: </dt>
																<dd>
																<a href="" rel="tag"> <?= l('capacidad') ?></a>
																</dd>
																<dt><?= l('Durada de la visita') ?>:</dt>
																<dd class="tribe-events-event-categories">
																<a href="<?= base_url() ?>" rel="tag"><?= l('duracion') ?></a>
																</dd>
																<dt><?= l('Idiomes') ?>:</dt>
																<dd class="tribe-event-tags">
																<a href="<?= base_url() ?>" rel="tag"><?= l('idiomas') ?></a>
																</dd>
																<dt> <?= l('Vins que es degusten') ?>: </dt>
																<dd class="tribe-events-event-url">
																<a href="#" target="_self"><?= l('eno1') ?></a>
																</dd>
																<dt><?= l('Preu de la visita amb tast') ?>: </dt>
																<dd class="tribe-events-event-url">
																<a href="#" target="_self"><?= l('eno2') ?></a>
																</dd>
															</dl>
														</div>
														<div class="col-xs-12 col-md-3">
															<h3 class="tribe-events-single-section-title"><?= l('Reserves') ?></h3>
															<dl>
																<dt style="display:none;"></dt>
																<dd class="tribe-organizer"><?= l('Per reservar una visita') ?>: </dd>
																<dd class="tribe-organizer-tel"><?= l('enoturisme_reserva_datos') ?></dd>
															</dl>
														</div>
														<div class="col-xs-12 col-md-6">
															<img src="<?= base_url('img/enoturisme/'.rand(1,4).'.jpg') ?>" alt="">
														</div>
													</div>
												</div>
												<div class="tribe-events-single-section tribe-events-event-meta secondary tribe-clearfix">
													<div class="tribe-events-meta-group tribe-events-meta-group-venue">
														<h3 class="tribe-events-single-section-title"> <?= l('Localització') ?> </h3>
														<dl>
															<dt></dt>
															<dd class="tribe-venue"> <?= l('On ens trobareu') ?>... </dd>
															<dt></dt>
															<dd class="tribe-venue-location">
															<address class="tribe-events-address">
																<span class="tribe-address">
																	<span class="tribe-street-address"><?= l('direccion') ?></span>
																	<br>
																	<!--
																	<span class="tribe-locality">Greenwich</span><span class="tribe-delimiter">,</span>
																<abbr class="tribe-region tribe-events-abbr" title="New York">NY</abbr>
																<span class="tribe-country-name">United States</span>
															</span>
															<a class="tribe-events-gmap" href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=3867+Robinson+Road+Greenwich+NY+United+States" title="Click to view a Google Map" target="_blank">+ Google Map</a>
															-->
														</address>
														</dd>
														<dt> <?= l('Localització geogràfica') ?>: </dt>
														<dd class="tribe-venue-tel"> ETRS89 * 0º86’71.25’’E  41º22’93.27’’N </dd>
													</dl>
												</div>
												<div class="tribe-events-venue-map" style="background:transparent; border:0">
													<!-- <div id="tribe-events-gmap-0" style="height: 350px; width: 100%"></div> -->
													<div id="sc_googlemap_1883708744" class="sc_googlemap sc_googlemap_detailed" style="width:100%;height:350px;" data-zoom="12" data-style="default">
														<div id="sc_googlemap_1883708744_1" class="sc_googlemap_marker" data-latlng="41.031786,-73.629356" data-address="" data-description="" data-title="Our Plantation" data-icon="<?= base_url() ?>theme/theme/images/googlemap_marker.png" style="width:100%; height:360px"></div>
													</div>
												</div>
											</div>
										</div>
										<div id="tribe-events-footer">
											<h3 class="tribe-events-visuallyhidden"><?= l('Event Navigation') ?></h3>
											<ul class="tribe-events-sub-nav">
												<li class="tribe-events-nav-previous"></li>
												<li class="tribe-events-nav-next"><a href="https://www.google.com/maps/place/Mas+d'en+Blei/@41.229314,0.8649803,17z/data=!3m1!4b1!4m5!3m4!1s0x12a13452d1ef9dff:0x9ba433debf96925d!8m2!3d41.229314!4d0.867169" target="_new"><?= l('Anar a Google Maps') ?> <span>&raquo;</span></a></li>
											</ul>
										</div>
									</div>
									<div class="tribe-events-after-html"></div>
								</div>
							</div>
						</div>
					</article>
					
					<article id="post-0" class="row tribe-events-single-section tribe-events-event-meta secondary tribe-clearfix" style="margin-left: 0; margin-right: 0;">
						<div class="col-xs-12 col-md-6">
							<div id="tribe-events-content" class="tribe-events-month">
								<div id="datetimepicker12"></div>
							</div>
						</div>
						<div class="col-xs-12 col-md-5 col-md-offset-1" style="margin-top:2em;" id="contacte">
							<h5 class="sc_item_subtitle sc_form_subtitle"><?= l('Contacta amb nosaltres') ?>  ...</h5>
							<form class="sc_form_form sc_input_hover_underline" method="post" action="" onsubmit="insertar('entradas/frontend/reservas/insert',this,'.result'); return false">
								<div class="sc_form_details trx_addons_columns_wrap">
									<div class="trx_addons_column-1_2">
										<label class="sc_form_field sc_form_field_name required">
											<span class="sc_form_field_wrap">
												<input type="text" name="nombre">
												<span class="sc_form_field_hover">
													<span class="sc_form_field_content" data-content="Name">
														<?= l('Nom i Cognoms'); ?>
													</span>
												</span>
											</span>
										</label>
									</div><div class="trx_addons_column-1_2">
										<label class="sc_form_field sc_form_field_email required">
											<span class="sc_form_field_wrap">
												<input type="text" name="email">
												<span class="sc_form_field_hover">
													<span class="sc_form_field_content" data-content="E-mail">
														<?= l('Email de contacte'); ?>
													</span>
												</span>
											</span>
										</label>
									</div>
								</div>
								<label class="sc_form_field sc_form_field_message required">
									<label class="sc_form_field sc_form_field_name required">
										<span class="sc_form_field_wrap">
											<input type="text" name="fecha">
											<span class="sc_form_field_hover">
												<span class="sc_form_field_content" data-content="Name">
													<?= l('Data de visita') ?>
												</span>
											</span>
										</span>
									</label>
								</label>
								<label class="sc_form_field sc_form_field_message required">
									<span class="sc_form_field_wrap">
										<textarea name="mensaje"></textarea>
										<span class="sc_form_field_hover">
											<span class="sc_form_field_content" data-content="Message"><?= l('Missatge'); ?></span>
										</span>
									</span>
								</label>
								<div class="result"></div>
								<div class="sc_form_field sc_form_field_button">
									<button type="submit"><?= l('Enviar missatge'); ?></button>
								</div>
								<div class="trx_addons_message_box sc_form_result"></div>
							</form>
						</div>
					</article>
				</div>
			</div>
		</div>
	<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>	</div>
</div>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyB_R8j4rq6MXoV17xrbyyh2rwZS6pTMd9w&libraries=drawing,geometry,places"></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/js/embedded-map.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/vendor/jquery-resize/jquery.ba-resize.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/the-events-calendar/src/resources/js/tribe-events.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>js/bootstrap.datepicker.ca.js'></script>
<script type='text/javascript' src='<?= base_url() ?>js/frame.js'></script>
<script>
	var URL = '<?= base_url() ?>';
	var opts = {
		center: new google.maps.LatLng(41.229318,0.8649803),
		zoom:16,
		styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
	};

	var mapa = new google.maps.Map(document.getElementById('sc_googlemap_1883708744_1'),opts);    	    	
	var mark = new google.maps.Marker({
		position:new google.maps.LatLng(41.229318,0.8649803),
		title:'Masdenblei',
		icon:'<?= base_url() ?>img/map-marker.png',
		map:mapa
	});

	var fechasBloqueadas = <?php 
		$x = array();
		foreach($this->elements->fechas_bloqueadas()->result() as $t){
			$x[] = $t->fecha;
		}
		echo json_encode($x);
	?>;
	$(function () {
        $('#datetimepicker12').datepicker({
            inline: true,
            sideBySide: true,
            startDate: "+1d",
            beforeShowDay:function(e){            	
            	var str = e.getFullYear()+'-';
            	var m = (e.getMonth()+1);
            	m = m<10?'0'+m:m;
            	str+= m+'-';
            	var d = e.getDate()<10?'0'+e.getDate():e.getDate();
            	str+=d;
            	if(fechasBloqueadas.indexOf(str)>-1){
            		return {enabled:false};
            	}
            }
        }).on('changeDate',function(e){
        	e = e.date;
        	var y = e.getFullYear();
        	var m = (e.getMonth()+1);
        	m = m<10?'0'+m:m;        	
        	var d = e.getDate()<10?'0'+e.getDate():e.getDate();        	
        	$("input[name='fecha']").val(d+'/'+m+'/'+y);
        });
    });
</script>
<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>

