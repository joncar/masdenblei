<div class="search_form_wrap">
	<form role="search" method="get" class="search_form" action="<?= base_url('paginas/frontend/search') ?>">
		<input type="text" class="search_field" placeholder="<?= l('Buscador...') ?>" value="" name="q">
		<button type="submit" class="search_submit icon-search" style="font-size: 1.2em;"></button>
		<a class="search_close icon-cancel"></a>
	</form>
</div>
<div class="idiomasMobile">
	<?= $this->load->view($this->theme.'_idiomas',array(),TRUE,'paginas') ?>
</div>