<body class="cellerPage page page-id-36 page-template-default body_tag body_style_wide scheme_default blog_mode_page is_single sidebar_hide expand_content remove_margins header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('QUI SOM') ?></h1>
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= l('QUI SOM') ?></span></div>
						</div>
					</div>
				</div>
				<div class="post_featured post_featured_fullwide" style="background-image:url(<?= base_url() ?>theme/theme/images/image-14.jpg);"></div>
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search') ?>
					</div>
					<div class="socials_mobile">
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
					</div>
				</div>
			</div>
		</div>
	
	<div class="page_content_wrap scheme_default">
		<div class="wrap">
			<div class="row">
				<div class="wpb_column vc_column_container column-2_12">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper"></div>
					</div>
				</div>
				<div class="wpb_column vc_column_container column-8_12">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
							<div class="vc_empty_space  height_tiny" style="height: 0px"></div>
							<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
								<div class="scheme_default sc_promo_text" style="width: 100%;">
									<div class="post_item post_layout_chess post_layout_chess_1 sc_promo_text_inner sc_align_center">
										<div class="sc_promo_icon" data-icon="icon-grape-3"><span class="icon-grape-3"></span></div>
										<?= l('elceller1') ?>
										<div class="post_meta"> 
											<span class="post_meta_item post_categories">
												<a href="#" rel="category tag"></a>
												<a href="#" rel="category tag"></a>
											</span>
										</div>
										<div class="sc_promo_content sc_item_content">
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<?= l('elceller2') ?>
												</div>
											</div>
										</div>
										<div class="sc_promo_button_image sc_item_button_image">
											<img src="<?= base_url() ?>theme/theme/images/sinature.png" alt="" width="297" height="105">
										</div>
									</div>
								</div>
							</div>
							<div class="vc_empty_space  height_large hide_on_mobile" style="height: 0px"></div>
							<div class="vc_empty_space  height_tiny" style="height: 0px"></div>
						</div>
					</div>
				</div>
				
				<div class="wpb_column vc_column_container column-2_12">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row vc_column-gap-30">
			<div class="wpb_column vc_column_container column-1_12">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper"></div>
				</div>
			</div>
			<div class="wpb_column vc_column_container column-5_12">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper">
						<div class="wpb_single_image wpb_content_element vc_align_left">
							<figure class="wpb_wrapper vc_figure">
								<a href="<?= base_url() ?>theme/theme/images/image-17-1024x7382.jpg" target="_self" class="vc_single_image-wrapper   vc_box_border_grey">
									<img width="1024" height="738" src="<?= base_url() ?>theme/theme/images/image-17-1024x7382.jpg" class="vc_single_image-img attachment-large" alt="image-17" />
								</a>
							</figure>
						</div>
					</div>
				</div>
			</div>
			<div class="wpb_column vc_column_container column-5_12">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper">

						<div class="wpb_single_image wpb_content_element vc_align_left">
							<figure class="wpb_wrapper vc_figure">
								<a href="<?= base_url() ?>theme/theme/images/image-18-1024x6832.jpg" target="_self" class="vc_single_image-wrapper   vc_box_border_grey"><img width="1024" height="683" src="<?= base_url() ?>theme/theme/images/image-18-1024x6832.jpg" class="vc_single_image-img attachment-large" alt="image-18" /></a>
							</figure>
						</div>
					</div>
				</div>
			</div>
			<div class="wpb_column vc_column_container column-1_12">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="wpb_column vc_column_container column-12_12">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper">
						<div class="vc_empty_space  height_large hide_on_mobile" style="height: 0px"></div>
						<div class="vc_empty_space  height_medium" style="height: 0px"></div>
					</div>
				</div>
			</div>
		</div>
		
		<article class="scheme_dark wpb_row post_item post_layout_chess post_layout_chess_1 post has-post-thumbnail" id="post_75" style="margin-bottom: 0px !important">
			<a id="sc_anchor_post_75" class="sc_anchor" title="" data-icon="" data-url=""></a>
			<div class="post_featured with_thumb post_featured_bg" style="background-image: url(<?= base_url() ?>theme/theme/images/image-6b.jpg); height: 479px;">
				<div class="mask"></div>
			</div>
			<div class="post_inner">
				<div class="post_inner_content">
					
					
					<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
						<div class="sc_promo_text" style="width: 100%;">
							<div class="sc_promo_text_inner sc_align_center">
								
								<?= l('elceller3') ?>
								<div class="sc_promo_icon celler" data-icon="icon-grape-3">									
									<img src="<?= base_url('img/logo-celler.svg') ?>" alt="">
								</div>								
							</div>
						</div>
						
					</div>
					
					
					
					
					
				</div>
			</div>
			
		</article>
		
		<div class="page_content_wrap scheme_default">
			<div class="">
				<div class="content">
					<div class="columns_wrap posts_container cuadrosCeller" style="margin-right:0">
						
						<div class="cellerCuadrosTextos column-1_2" style="background:#f7f7f7;">
							<article class="post_item post_layout_classic post_layout_classic_2 type-post has-post-thumbnail" data-animation="animated fadeInUp normal" style="margin-bottom: 0">
								<div class="post_content entry-content" style="padding: 0 60px; margin-bottom: 0">
									<div class="post_content_inner">
										<div style="text-align: center">
											<h4 class="sc_testimonials_item_author_title"><?= l('Autenticitat') ?></h4>
										</div>
										<div class="sc_promo_content sc_item_content">
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<?= l('elceller4') ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</article>
						</div><div class="cellerCuadrosTextos column-1_2" style="background:#e9e7e7;">
						<article class="post_item post_layout_classic post_layout_classic_2 type-post has-post-thumbnail" data-animation="animated fadeInUp normal" style="margin-bottom: 0">
							<div class="post_content entry-content" style="padding: 0 60px; margin-bottom: 0">
								<div class="post_content_inner">
									<div style="text-align: center"><h4 class="sc_testimonials_item_author_title"><?= l('Tecnologia al servei de la tradició') ?> </h4></div>
									<div class="sc_promo_content sc_item_content">
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
												<?= l('elceller5') ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div><div class="cellerCuadrosTextos column-1_2" style="background:#e9e7e7;">
					<article class="post_item post_layout_classic post_layout_classic_2 type-post has-post-thumbnail" data-animation="animated fadeInUp normal" style="margin-bottom: 0">
						<div class="post_content entry-content" style="padding: 0 60px; padding-top:0">
							<div class="post_content_inner">
								<div style="text-align: center"><h4 class="sc_testimonials_item_author_title"><?= l('Cura pel detall') ?> </h4></div>
								<div class="sc_promo_content sc_item_content">
									<div class="wpb_text_column wpb_content_element ">
										<div class="wpb_wrapper">
											<?= l('elceller6') ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</article>
				</div><div class="cellerCuadrosTextos column-1_2" style="background:#f7f7f7;">
				<article class="post_item post_layout_classic post_layout_classic_2 type-post has-post-thumbnail" data-animation="animated fadeInUp normal" style="margin-bottom: 0">
					<div class="post_content entry-content" style="padding: 0 60px; padding-top:0">
						<div class="post_content_inner">
							<div style="text-align: center">
								<h4 class="sc_testimonials_item_author_title"><?= l('Complicitat amb la Natura') ?></h4>
							</div>
							<div class="sc_promo_content sc_item_content">
								<div class="wpb_text_column wpb_content_element ">
									<div class="wpb_wrapper">
										<?= l('elceller7') ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
</div>
</div>


















<div class="wrap cellerEquipo">
<div class="row">
	
	<div class="wpb_column vc_column_container column-12_12">
		<div class="vc_column-inner ">
			<div class="wpb_wrapper">
				<div class="vc_empty_space  height_medium" style="height: 0px"></div>
				<div class="wpb_wrapper">
					<div class="vc_empty_space  height_tiny" style="height: 0px"></div>
					<div class="sc_promo sc_promo_default sc_promo_size_normal sc_promo_no_paddings sc_promo_no_image">
						<div class="sc_promo_text" style="width: 100%;">
							<div class="sc_promo_text_inner sc_align_center">
								<div class="sc_promo_icon" data-icon="icon-grape-3"><span class="icon-grape-3"></span></div>
								<h2 class="sc_item_title sc_promo_title"><?= l('LEQUIP') ?></h2>
								<h5 class="sc_item_subtitle sc_promo_subtitle">“<?= l('Treballar en equip divideix el treball i multiplica els resultats') ?>”</h5>
								<div class="sc_promo_content sc_item_content">
									
								</div>
								
							</div>
						</div>
					</div>
					<div class="vc_empty_space  height_large hide_on_mobile" style="height: 0px"></div>
					
				</div>
				
				<div class="row">
					<div class="wpb_column vc_column_container column-12_12">
						<div class="vc_column-inner ">
							<div class="equipsub wrap">
								
								
								<div class="woocommerce">
									<div class="products row">
										<div class="col-xs-12 col-md-12">
											<div class="product has-post-thumbnail first col-xs-12 col-md-5">
												<div class="post_item post_layout_thumbs">
													<div class="post_featured">
														<a href="javascript:void(0)">
															<img src="<?= base_url() ?>theme/theme/images/2.jpg" alt="kaO0w6Nl-FineWines_CentenaryHill_Shiraz_43x2" title="kaO0w6Nl-FineWines_CentenaryHill_Shiraz_43x2" width="360" height="480">
														</a>
													</div>													
												</div>
											</div>
											<div class="product has-post-thumbnail col-xs-12 col-md-5">
												<div class="post_content_inner">
													<ul class="products">
														<li class="product has-post-thumbnail first" style="padding: 0 0 20px 0;">
															<div class="post_layout_thumbs">
																
																<div class="post_data">
																	<div class="post_header entry-header">
																		<div class="post_tags product_tags"><a href="product.html" rel="tag">Jaume Ferrús</a></div>
																		<h3>
																			<a href="javascript:void(0)"><?= l('Propietari') ?></a>
																		</h3>
																	</div>
																</div>
															</div>
														</li>
													</ul>
													<div class="sc_promo_content sc_item_content">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<?= l('elceller8') ?>
															</div>
														</div>
													</div>
												</div>
												<div class="post_content entry-content">
													<img class="firma" src="<?= base_url() ?>theme/theme/images/sinaturejf.png" alt="">
												</div>
											</div>
										</div>



										<div class="col-xs-12 col-md-12">
											<div class="product has-post-thumbnail first col-xs-12 col-md-5">
												<div class="post_item post_layout_thumbs">
													<div class="post_featured">
														<a href="javascript:void(0)">
															<img src="<?= base_url() ?>theme/theme/images/equip2.jpg" alt="kaO0w6Nl-FineWines_CentenaryHill_Shiraz_43x2" title="kaO0w6Nl-FineWines_CentenaryHill_Shiraz_43x2" width="360" height="480">
														</a>
													</div>													
												</div>
											</div>
											<div class="product has-post-thumbnail col-xs-12 col-md-5">
												<div class="post_content_inner">
													<ul class="products">
														<li class="product has-post-thumbnail first" style="padding: 0 0 20px 0;">
															<div class="post_layout_thumbs">
																
																<div class="post_data">
																	<div class="post_header entry-header">
																		<div class="post_tags product_tags"><a href="product.html" rel="tag">Jordi Torrella</a></div>
																		<h3>
																			<a href="javascript:void(0)"><?= l('Enòleg') ?></a>
																		</h3>
																	</div>
																</div>
															</div>
														</li>
													</ul>
													<div class="sc_promo_content sc_item_content">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<?= l('elceller9') ?>
															</div>
														</div>
													</div>
												</div>
												<div class="post_content entry-content">
													<img class="firma" src="<?= base_url() ?>theme/theme/images/sinature-jt.png" alt="">
												</div>
											</div>
										</div>

										

										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
				
			</div>
		</div>
	</div>
</div>
</div>

<div class="row vc_custom_1465549560104">
<div class="wpb_column vc_column_container column-12_12">
	<div class="vc_column-inner ">
		<div class="wrap">
			<div class="vc_empty_space  height_huge" style="height: 0px"></div>
			<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
			<div class="wpb_text_column wpb_content_element ">
				<div class="wpb_wrapper">
					<h3 style="text-align: center;">
						<span style="color: #ffffff;">
							<?= l('elceller10') ?>
						</span>
					</h3>
				</div>
			</div>
			<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
			<div class="vc_empty_space  height_huge" style="height: 0px"></div>
		</div>
	</div>
</div>
</div>
</div>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
</div>
</div>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/swiper/swiper.jquery.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/magnific/jquery.magnific-popup.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/trx_addons.js'></script>

<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>