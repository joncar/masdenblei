<body class="single single-product postid-403 woocommerce woocommerce-page page page-id-296 page-template-default body_tag body_style_wide scheme_default blog_mode_page is_single sidebar_hide expand_content remove_margins header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive  vins">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							
						</div>
					</div>
				</div>				
			</header>

		<div class="menu_mobile_overlay"></div>
		<div class="menu_mobile scheme_dark">
			<div class="menu_mobile_inner">
				<a class="menu_mobile_close icon-cancel"></a>
				<nav class="menu_mobile_nav_area">
					<ul id="menu_mobile" class="menu_mobile_nav">
						<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
					</ul>
				</nav>
				<div class="search_mobile">
					<?= $this->load->view($this->theme.'_search',array(),TRUE,'paginas') ?>
				</div>
				<div class="socials_mobile">
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
					<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
				</div>
			</div>
		</div>
		<div class="page_content_wrap scheme_default">
			<div class="content">
				<article class="viDetail post_item">
					<div class="col-l p">
						<div class="post_inner">
							<div class="post_inner_content">
								<div class="post_header entry-header">
									<h1 class="page_caption">
										<div class="post_meta post_meta_sin_raya"> 
											<span class="post_meta_item post_categories">
												<a href="#" rel="category tag"></a>
												<a href="#" rel="category tag"><?= $detail->categoria ?></a>
											</span>
											<span class="post_meta_item post_date">
												<a href="#"><?= $detail->anyo ?></a>
											</span>
										</div>
										<?= $detail->nombre ?>
									</h1>								
									<h3 class="post_title entry-title"><a href="javascript:void(0)" rel="bookmark"></a></h3>
									<div class="post_meta"> 
										<span class="post_meta_item post_categories">
											<a href="#" rel="category tag"></a>
											<a href="#" rel="category tag"></a>
										</span>
									</div>	
								</div>
								<div class="post_content entry-content">								
									<ul class="ficha_detalle">
										<li class="recentcomments"><span class="comment-author-link"><?= l('Denominació dorigen') ?>:</span><a href="javascript:void(0)"> <?= $detail->origen ?></a></li>
										<li class="recentcomments"><span class="comment-author-link"><?= l('Tipologia') ?>:</span><a href="javascript:void(0)"> <?= $detail->tipologia ?></a></li>
										<li class="recentcomments"><span class="comment-author-link"><?= l('Varietats') ?>:</span><a href="javascript:void(0)"> <?= $detail->varietats ?></a></li>
										<li class="recentcomments"><span class="comment-author-link"><?= l('Anyada') ?>:</span><a href="javascript:void(0)"> <?= $detail->anyo ?></a></li>
										<li class="recentcomments"><span class="comment-author-link"><?= l('Enòleg') ?>: </span><a href="javascript:void(0)"> <?= $detail->enoleg ?></a></li>
										<li class="recentcomments"><span class="comment-author-link"><?= l('Criança') ?>:</span><a href="javascript:void(0)"> <?= $detail->crianca ?></a></li>
										<li class="recentcomments"><span class="comment-author-link"><?= l('Tap') ?>:</span><a href="javascript:void(0)"> <?= $detail->tap ?></a></li>
										<li class="recentcomments"><span class="comment-author-link"><?= l('Producció') ?>:</span><a href="javascript:void(0)"> <?= $detail->produccio ?></a></li>
									</ul>
									<div class="post_meta"> <span class="post_meta_item post_categories"></span></div>									
									<h4 class="post_title entry-title">
										<a href="javascript:void(0)" rel="bookmark"><?= l('Viticultura') ?></a>
									</h4>									
									<?= $detail->viticultura ?>
								</div>									
							</div>
						</div>
					</div>
					<div class="col-2 bg" style="background-image: url(<?= $detail->foto ?>); background-size:cover; background-repeat: no-repeat; background-position: center"></div>
				</article>

				<article class="viDetail post_item">
					<div class="col-2 bg" style="background-image: url(<?= $detail->foto_lateral ?>); background-size:cover; background-repeat: no-repeat; background-position: center"></div>

					<div class="col-l p">
						<div class="post_inner">
							<div class="post_inner_content">
							
								
									
									
								<div class="post_content entry-content">
									<div class="">
										<h4 class="post_title entry-title">
											<?= l('Vinificació') ?>
										</h4>
										<?= $detail->vinificacio ?>
									</div>
								</div>

								<div class="post_header entry-header">
									<div class="post_meta"> 
										<span class="post_meta_item post_categories"></span>
									</div>									
								</div>
									
									
								<div class="post_content entry-content">
									<div class="">
										<h4 class="post_title entry-title">
											<?= l('Notes de tast') ?>
										</h4><br>
										<?= $detail->notes_tast ?>
									</div>								
								</div>
								<div class="post_header entry-header">
									<div class="post_meta"> <span class="post_meta_item post_categories"></span></div>																	
								</div>
									
									
								<div class="post_content entry-content">
									<div class="">
										<h4 class="post_title entry-title">
											<?= l('Recomanacions a lhora de servir-lo') ?>
										</h4>
										<?= $detail->recomanacions ?>
									</div>								
								</div>
								
							</div>
					</div>
					
				</article>				

			</div>

			<div class="wrap">
				<div class="content">
								
					<div class="related products">
					<h2><?= l('Altres productes') ?></h2>
					
					<ul class="products vinosRelateds">

						<?php foreach($recomendados->result() as $r): ?>
							<li class="product type-product has-post-thumbnail column-1_4 first">
								<div class="post_item post_layout_thumbs">
									<div class="post_featured hover_shop">
										<a href="<?= $r->link ?>">
											<img width="360" height="480" src="<?= $r->foto ?>" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="<?= $r->nombre ?>" title="<?= $r->nombre ?>"/> </a>
										<div class="mask"></div>
										<div class="icons">
											<a rel="nofollow" href="<?= $r->link ?>" aria-hidden="true" data-quantity="1" data-product_id="401" data-product_sku="" class="shop_cart icon-shopping-cart button add_to_cart_button product_type_simple ajax_add_to_cart"></a>
											<a href="<?= $r->link ?>" aria-hidden="true" class="shop_link icon-link"></a>
										</div>
									</div>
									<div class="post_data">
										<div class="post_header entry-header">
											<div class="post_tags product_tags">
												<a href="<?= $r->link ?>" rel="tag"><?= $r->anyo ?></a>
											</div>
											<h3><a href="<?= $r->link ?>"><?= $r->nombre ?></a></h3>
										</div>										
										<a rel="nofollow" href="<?= $r->link ?>" data-quantity="1" data-product_id="401" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart"><?= l('Veure') ?></a> 
									</div>
								</div>
							</li>
						<?php endforeach ?>
						

					</ul>
				</div>		
				</div>
			</div>
			<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>												
		</div>
		<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
	</div>
	</div>
	<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>	
	<a href="javascript:void(0)" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>