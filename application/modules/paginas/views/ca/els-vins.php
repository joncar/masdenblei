<body class="page page-id-296 page-template-default body_tag body_style_wide scheme_default blog_mode_page is_single sidebar_hide expand_content remove_margins header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('vins Els nostres vins') ?></h1>
								
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= l('vins Llista de vins') ?></span></div>
						</div>
					</div>
				</div>
				<h5 class="sc_item_subtitle sc_promo_subtitle">“<?= l('Hi ha la veritat en el vi') ?>” <br>  </h5>
				<div class="post_featured post_featured_fullwide" style="background-image:url(<?= base_url() ?>theme/theme/images/d3.jpg); margin-bottom: 0"></div>
			</header>
			
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search') ?>
					</div>
					<div class="socials_mobile">
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
					</div>
				</div>
			</div>
			<div class="page_content_wrap scheme_default">
				<div class="content_wrap">
					<div class="content">
						<article id="post-296" class="post_item_single post_type_page post-296 page type-page">
							<div class="post_content entry-content">
								



								<?php foreach($this->elements->vinos()->result() as $n=>$v): ?>
									<div class="row row-o-full-height row-o-columns-stretch row-o-equal-height">
										<div class="wpb_column vc_column_container column-12_12">
											<div class="vc_column-inner ">
												<div class="wpb_wrapper">
													<a id="sc_anchor_cabernet_sauvignon_2012" class="sc_anchor" title="Cabernet Sauvignon 2012" data-icon="" data-url=""></a>
													<div class="sc_promo sc_promo_default sc_promo_size_large sc_promo_image_fit">
														
														<div class="sc_promo_image" style="background-color:#f7f7f5;background-image:url(<?= $v->foto ?>);<?= $n%2==0?'right: 0;':'left:0;' ?>"></div>
														<div class="sc_promo_text trx_addons_stretch_height" style="width: 50%;<?= $n%2==0?'float: left;':'float:right;' ?>">
															<div class="sc_promo_text_inner">
																<h2 class="sc_item_title sc_promo_title"><small><?= $v->anyo ?></small><br><?= $v->nombre ?></h2>
																
																<div class="sc_promo_content sc_item_content" >
																	<ul class="ficha_detalle post_content_inner">
																		<li class="recentcomments"><span class="comment-author-link"><?= l('Denominació dorigen') ?>:</span><a href="javascript:void(0)"> <?= $v->origen ?></a></li>
																		<li class="recentcomments"><span class="comment-author-link"><?= l('Tipologia') ?>:</span><a href="javascript:void(0)"> <?= $v->tipologia ?></a></li>
																		<li class="recentcomments"><span class="comment-author-link"><?= l('Varietats') ?>:</span><a href="javascript:void(0)"> <?= $v->varietats ?></a></li>
																		<li class="recentcomments"><span class="comment-author-link"><?= l('Anyada') ?>:</span><a href="javascript:void(0)"> <?= $v->anyo ?></a></li>
																		<li class="recentcomments"><span class="comment-author-link"><?= l('Enòleg') ?>: </span><a href="javascript:void(0)"> <?= $v->enoleg ?></a></li>
																		<li class="recentcomments"><span class="comment-author-link"><?= l('Criança') ?>:</span><a href="javascript:void(0)"> <?= $v->crianca ?></a></li>
																		<li class="recentcomments"><span class="comment-author-link"><?= l('Tap') ?>:</span><a href="javascript:void(0)"> <?= $v->tap ?></a></li>
																		<li class="recentcomments"><span class="comment-author-link"><?= l('Producció') ?>:</span><a href="javascript:void(0)"> <?= $v->produccio ?></a></li>
																	</ul>
																</div>
															
																<div class="sc_align_center" style="margin-top:50px;">
																	<div class="sc_item_button sc_promo_button sc_button_wrap">
																		<a href="<?= $v->link ?>" class="sc_button sc_button_simple sc_button_size_normal sc_button_icon_top">
																			<span class="sc_button_icon">
																				<span class="icon-down-big"></span>
																			</span>
																			<span class="sc_button_text">
																				<span class="sc_button_title"><?= l('Mès sobre aquest vi') ?></span>
																			</span> 
																		</a>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach ?>

								
							</div>
						</article>
					</div>
				</div>
			</div>
			<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
		</div>
	</div>
	<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>	
	<a href="wine-list.html#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>