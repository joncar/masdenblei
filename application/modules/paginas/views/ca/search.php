<body class="page page-id-299 page-template-default body_tag body_style_wide scheme_default blog_mode_page is_single sidebar_hide header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('Resultados de busqueda') ?></h1>
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= l('Resultados de busqueda') ?></span></div>
						</div>
					</div>
				</div>
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<div class="menu_mobile_inner">
						<a class="menu_mobile_close icon-cancel"></a>
						<nav class="menu_mobile_nav_area">
							<ul id="menu_mobile" class="menu_mobile_nav">
								<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
							</ul>
						</nav>
						<div class="search_mobile">
							<?= $this->load->view($this->theme.'_search') ?>
						</div>
						<div class="socials_mobile">
							<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
							<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
							<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
						</div>
					</div>
				</div>
			</div>
			<div class="page_content_wrap scheme_default">
				<div class="content_wrap">
					<div class="post_content entry-content">
						<section>
							<div class="container">
								<div class="row">			
									<div class="col-xs-12 col-sm-12 col-md-12 about-1">
										<?= $resultado ?>
									</div>
								</div>
								<!-- .row end -->
							</div>
							<!-- .container end -->
						</section>
					</div>
				</div>
			</div>
		<!-- </.page_content_wrap> -->
		<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
		<!-- /.site_footer_wrap -->
	</div>
</div>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
	<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>