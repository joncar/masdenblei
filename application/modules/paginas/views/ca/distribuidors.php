<?php
	$europa = $this->elements->distribuidores(array('region'=>1));
	$america = $this->elements->distribuidores(array('region'=>2));
?>
<body class="cellerPage page page-id-36 page-template-default body_tag body_style_wide scheme_default blog_mode_page is_single sidebar_hide expand_content remove_margins header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive distribuidors">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('DISTRIBUÏDORS') ?></h1>
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= l('DISTRIBUÏDORS') ?></span></div>
						</div>
					</div>
				</div>	
				<div class="post_featured post_featured_fullwide" style="background-image:url(<?= base_url() ?>theme/theme/images/d1.jpg); margin:0"></div>			
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<nav class="menu_mobile_nav_area">
						<ul id="menu_mobile" class="menu_mobile_nav">
							<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
						</ul>
					</nav>
					<div class="search_mobile">
						<?= $this->load->view($this->theme.'_search') ?>
					</div>
					<div class="socials_mobile">
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
						<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
					</div>
				</div>
			</div>





<article class="wpb_row post_item post_layout_chess post_layout_chess_1 post has-post-thumbnail" id="post_75" style="margin-bottom:0 !important">
	<a id="sc_anchor_post_75" class="sc_anchor" title="How to Pair Different Wines with Your Favorite Food" data-icon="" data-url=""></a>
	<div id="europa" class="col-xs-12 col-sm-6 post_featured with_thumb post_featured_bg" style="height: 700px;"></div>
	<aside id="recent-comments-3" class=" equipoRow col-xs-12 col-sm-6 widget widget_recent_comments">
		<div class="row">
		<?php
			$x = 0;
			$paises = array();
			foreach($europa->result() as $n=>$e):
		?>
		<?php if($x==0): ?>
			<div class="col-xs-6 col-md-4">
		<?php endif; $x++; ?>
		
			
			
				<div class="nosotros_dir <?= !in_array($e->pais,$paises)?'':'nocuadro' ?> sc_googlemap_content sc_googlemap_content_detailed">
					<div class="wpb_text_column wpb_content_element ">
						<div class="wpb_wrapper">
							<?php if(!in_array($e->pais,$paises)): $paises[] = $e->pais; ?>
								<h5 class="widget_title paisnosotros" style="color: #e39a31;font-weight: 800;"><?= $e->pais ?></h5>						
							<?php endif ?>
							<h5 class="widget_title"><?= $e->nombre ?></h5>
							<p><a href="http://<?= $e->web ?>" target="_new"><?= $e->web ?></a></p>
						</div>
					</div>
				</div>


		<?php if($x==11 || $x==$europa->num_rows()): $x = 0;?>
			</div>
		<?php endif ?>
		<?php endforeach ?>
		</div>
		
	</aside>

</article>
<article class="wpb_row post_item post_layout_chess post_layout_chess_1 post has-post-thumbnail" id="post_75">
	<a id="sc_anchor_post_75" class="sc_anchor" title="How to Pair Different Wines with Your Favorite Food" data-icon="" data-url=""></a>
	<div id="america" class="col-xs-12 col-sm-6 post_featured with_thumb post_featured_bg"  style="height: 700px;">></div>
	<aside id="recent-comments-3" class="equipoRow col-xs-12 col-sm-6 widget widget_recent_comments">
		
		<div class="row">
		<?php
			$x = 0;
			$paises = array();
			foreach($america->result() as $n=>$e):
		?>
		<?php if($x==0): ?>
			<div class="col-xs-6 col-md-4">
		<?php endif; $x++; ?>
		
			
			
				<div class="nosotros_dir  <?= !in_array($e->pais,$paises)?'':'nocuadro' ?> sc_googlemap_content sc_googlemap_content_detailed">
					<div class="wpb_text_column wpb_content_element ">
						<div class="wpb_wrapper">
							<?php if(!in_array($e->pais,$paises)): $paises[] = $e->pais;  ?>
								<h5 class="widget_title paisnosotros" style="color: #e39a31;font-weight: 800;"><?= $e->pais ?></h5>						
							<?php endif ?>
							<h5 class="widget_title"><?= $e->nombre ?></h5>
							<p><a href="http://<?= $e->web ?>" target="_new"><?= $e->web ?></a></p>
						</div>
					</div>
				</div>


		<?php if($x==$america->num_rows() || !in_array($america->row($n+1)->pais,$paises)): $x = 0;?>
			</div>
		<?php endif ?>
		<?php endforeach ?>
		</div>
		
	</aside>

</article>






</div>
<div class="row vc_custom_1465549560104">
	<div class="wpb_column vc_column_container column-12_12">
		<div class="vc_column-inner ">
			<div class="wrap">
				<div class="vc_empty_space  height_huge" style="height: 0px"></div>
				<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
				<div class="wpb_text_column wpb_content_element ">
					<div class="wpb_wrapper">
						<h3 style="text-align: center;"><span style="color: #ffffff;"><?= l('distribuidors1') ?></span></h3>
					</div>
				</div>
				<div class="vc_empty_space  height_huge hide_on_mobile" style="height: 0px"></div>
				<div class="vc_empty_space  height_huge" style="height: 0px"></div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
</div>
</div>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/swiper/swiper.jquery.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/magnific/jquery.magnific-popup.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/vendor/plugins/trx_addons/js/trx_addons.js'></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyB_R8j4rq6MXoV17xrbyyh2rwZS6pTMd9w&libraries=drawing,geometry,places"></script> 
<script>
	var opts = {
			center: new google.maps.LatLng(49.7315025,4.1203053),
			zoom:4.7,
			styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
		};
		var opts2 = {
			center: new google.maps.LatLng(40.0213763,-92.7297203),
			zoom:4,
			styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
		};
	var europa = new google.maps.Map(document.getElementById('europa'),opts);
	var america = new google.maps.Map(document.getElementById('america'),opts2);
<?php foreach($europa->result() as $e): ?>
new google.maps.Marker({
map:europa,
title:'<?= $e->nombre ?>',
position:new google.maps.LatLng(<?= $e->lat ?>,<?= $e->lng ?>),
icon:'<?= base_url('img/pin.png') ?>'
});
<?php endforeach ?>
<?php foreach($america->result() as $e): ?>
new google.maps.Marker({
map:america,
title:'<?= $e->nombre ?>',
position:new google.maps.LatLng(<?= $e->lat ?>,<?= $e->lng ?>),
icon:'<?= base_url('img/pin.png') ?>'
});
<?php endforeach ?>
</script>
<script>
	jQuery(".rosa, .mapaCeller li").on('mouseover',function(){
		/*jQuery(this).addClass('activado')*/
		var region = jQuery(this).data('region');
		jQuery('.mapaCeller li[data-region="'+region+'"]').addClass('active');
		jQuery('.rosa[data-region="'+region+'"]').addClass('activado');
	});
	jQuery(".rosa, .mapaCeller li").on('mouseout',function(){
		/*jQuery(this).removeClass('activado')*/
		var region = jQuery(this).data('region');
		jQuery('.mapaCeller li[data-region="'+region+'"]').removeClass('active');
		jQuery('.rosa[data-region="'+region+'"]').removeClass('activado');
	});
</script>
<a href="about-us.html#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>