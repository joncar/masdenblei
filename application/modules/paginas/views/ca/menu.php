<li class="menu-item">
	<a href="<?= base_url() ?>"><span><?= l('INICI') ?></span></a>
</li>
<li class="menu-item menu-item-has-children">
	<a href="#">
		<span><?= l('QUI SOM') ?></span>
	</a>
	<ul class="sub-menu">
		<li class="menu-item"><a href="<?= base_url() ?>el-celler.html"><span><?= l('El Celler'); ?></span></a></li>
		<li class="menu-item"><a href="<?= base_url() ?>el-celler.html"><span><?= l('Història'); ?></span></a></li>
		<li class="menu-item"><a href="<?= base_url() ?>el-celler.html"><span><?= l('Equip'); ?></span></a></li>		
	</ul>
</li>

<li class="menu-item">
	<a href="<?= base_url() ?>les-vinyes.html">
		<span><?= l('Les Vinyes') ?></span>
	</a>	
</li>
<li class="menu-item menu-item-has-children">
	<a href="<?= base_url() ?>els-vins.html"><span><?= l('ELS VINS') ?></span></a>
	<ul class="sub-menu">
		<?php foreach($this->elements->vinos()->result() as $v): ?>
			<li class="menu-item"><a href="<?= $v->link ?>"><span><?= $v->nombre ?></span></a></li>
		<?php endforeach ?>
		<li class="menu-item"><a href="<?= base_url() ?>vins-experimentals.html"><span><?= l('Vins Experimentals') ?></span></a></li>
	</ul>
</li>
<li class="menu-item">
	<a href="<?= base_url() ?>distribuidors.html"><span><?= l('distribuidores') ?></span></a>
</li>
<li class="menu-item menu-item-has-children">
	<a href="<?= base_url() ?>blog.html">
		<span><?= l('ACTUALITAT') ?></span>
	</a>
	<ul class="sub-menu">
		<li class="menu-item"><a href="<?= base_url() ?>blog.html"><span><?= l('Notícies') ?></span></a></li>
		<li class="menu-item"><a href="<?= base_url() ?>premis.html"><span><?= l('Premis') ?></span></a></li>
	</ul>
</li>
<?php if($this->db->get('ajustes')->row()->enoturisme==1): ?>
<li class="menu-item"><a href="<?= base_url() ?>enoturisme.html"><span><?= l('Enoturisme'); ?></span></a></li>
<?php endif ?>
<li class="menu-item"><a href="<?= base_url() ?>galeria.html"><span><?= l('GALERIA'); ?></span></a></li>
<li class="menu-item"><a href="<?= base_url() ?>contacte.html"><span><?= l('CONTACTE'); ?></span></a></li>