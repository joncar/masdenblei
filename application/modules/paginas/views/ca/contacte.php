<body class="page page-id-299 page-template-default body_tag body_style_wide scheme_default blog_mode_page is_single sidebar_hide header_style_header-1 header_position_default header_title_on menu_style_top no_layout vc_responsive">
	<div class="body_wrap">
		<div class="page_wrap">
			<header class="top_panel top_panel_style_1 scheme_default" style="background:url(<?= base_url() ?>theme/theme/images/image-101.jpg); background-size: contain;">
				<a class="menu_mobile_button"></a>
				<div class="top_panel_fixed_wrap"></div>
				<?php $this->load->view('_menu_interno'); ?>
				<div class="top_panel_title_wrap">
					<div class="content_wrap">
						<div class="top_panel_title">
							<div class="page_title">
								<h1 class="page_caption"><?= l('Contacte') ?></h1>
							</div>
							<div class="breadcrumbs"><a class="breadcrumbs_item home" href="<?= base_url() ?>"><?= l('INICI') ?></a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current"><?= l('Contacte') ?></span></div>
						</div>
					</div>
				</div>
			</header>
			<div class="menu_mobile_overlay"></div>
			<div class="menu_mobile scheme_dark">
				<div class="menu_mobile_inner">
					<a class="menu_mobile_close icon-cancel"></a>
					<div class="menu_mobile_inner">
						<a class="menu_mobile_close icon-cancel"></a>
						<nav class="menu_mobile_nav_area">
							<ul id="menu_mobile" class="menu_mobile_nav">
								<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
							</ul>
						</nav>
						<div class="search_mobile">
							<?= $this->load->view($this->theme.'_search') ?>
						</div>
						<div class="socials_mobile">
							<span class="social_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="trx_addons_icon-twitter"></span></a></span>
							<span class="social_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="trx_addons_icon-facebook"></span></a></span>
							<span class="social_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="trx_addons_icon-gplus"></span></a></span>
						</div>
					</div>
				</div>
			</div>
			<div class="page_content_wrap scheme_default">
				<div class="content_wrap">
					<div class="post_content entry-content">
						<div class="wpb_wrapper">
							<div id="sc_googlemap_1883708744_wrap" class="sc_googlemap_wrap">
								<div id="sc_googlemap_1883708744" class="sc_googlemap sc_googlemap_detailed" style="width:100%;height:660px;" data-zoom="12" data-style="default">
									<div id="mapa" class="sc_googlemap_marker" data-latlng="41.229318,0.8649803" data-address="Mas d’en Blei s/n, 43361 La Morera de Montsant (Tarragona)" data-description="" data-title="Our Plantation" data-icon="<?= base_url() ?>img/map-marker.png" style="width:100%; height:100vh;"></div>
								</div>
								<div class="sc_googlemap_content sc_googlemap_content_detailed">
									<div class="wpb_text_column wpb_content_element ">
										<div class="wpb_wrapper">
											<p><em><strong><?= l('Adreça') ?>:</strong></em><br/> <?= l('direccion') ?></p>
											<p><em><strong><?= l('Telèfon') ?>:</strong></em><br/> +34 686 246 679</p>
											<p><em><strong>E-mail:</strong></em><br/>  info@masdenblei.com</p>																		
													
									</div>
								</div>
							</div>
							<!-- /.sc_googlemap_wrap -->
						</div>
					</div>
				</div>
				<div class="wrap">
					<div class="row">
							<div class="wpb_column vc_column_container column-12_12">
								<div class="vc_column-inner ">
									<div class="content">
										<div class="vc_empty_space  height_medium" style="height: 0px"></div>
										<div class="sc_icons sc_icons_default sc_icons_size_large sc_align_center">
											<div class="sc_icons_item">
												<div class="sc_icons_icon sc_icons_icon_type_fontawesome icon-winery"><span class="sc_icons_icon_type_fontawesome icon-winery"></span></div>
											</div>
										</div>
										<!-- /.sc_icons -->
										<div class="sc_align_center">
											<h2 class="sc_item_title sc_form_title"><?= l('DADES DE CONTACTE') ?></h2>
											<h5 class="sc_item_subtitle sc_form_subtitle"> <br></h5>
											<form class="sc_form_form sc_input_hover_underline" method="post" action="paginas/frontend/contacto" onsubmit="sendForm(this,'',function(data){console.log(data);jQuery('.trx_addons_message_box').html(data).show(); jQuery('body').on('click',function(){jQuery('.trx_addons_message_box').hide();});}); return false;">
												<div class="sc_form_details trx_addons_columns_wrap">
													<div class="trx_addons_column-1_3"><label class="sc_form_field sc_form_field_name">
														<span class="sc_form_field_wrap"><input type="text" name="nombre"><span class="sc_form_field_hover"><span class="sc_form_field_content"><?= l('Nom i Cognoms'); ?></span></span></span></label>
													</div><div class="trx_addons_column-1_3"><label class="sc_form_field sc_form_field_name">
														<span class="sc_form_field_wrap"><input type="text" name="email"><span class="sc_form_field_hover"><span class="sc_form_field_content"><?= l('Email de contacte'); ?></span></span></span></label>
													</div><div class="trx_addons_column-1_3"><label class="sc_form_field sc_form_field_name">
														<span class="sc_form_field_wrap"><input type="text" name="telefono"><span class="sc_form_field_hover"><span class="sc_form_field_content"><?= l('Telèfon de contacte'); ?></span></span></span></label>
													</div>													

												</div><label class="sc_form_field sc_form_field_message"><span class="sc_form_field_wrap"><textarea name="extras[message]" aria-required="true"></textarea><span class="sc_form_field_hover"><span class="sc_form_field_content" data-content="Message"><?= l('Missatge'); ?></span></span>	</span></label>
												<div class="sc_form_field">
														<input type="checkbox" name="politicas" id="politicas" value="1">
														<label class="sc_form_field sc_form_field_name" for="politicas" style="text-align:left">
															<a href="<?= base_url().'politica-de-privacitat.html' ?>" target="_new"><?= l('acepto-las-politicas-de-privacidad'); ?></a>
														</label>
													</div>
												<div class=""><button  type="submit"><?= l('Enviar missatge'); ?></button></div>
												<div class="trx_addons_message_box sc_form_result"></div>
											</form>
										</div>
										<!-- /.sc_form -->
									</div>
								</div>
							</div>
						</div>
						<!-- .entry-content -->
					<!-- </.content> -->
				</div>
				<!-- </.content_wrap> -->
			</div>
			<!-- </.page_content_wrap> -->
			<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
			<!-- /.site_footer_wrap -->
		</div>
		<!-- /.page_wrap -->
	</div>
	<!-- /.body_wrap -->
	<!-- Post/page views count increment -->
	<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyB_R8j4rq6MXoV17xrbyyh2rwZS6pTMd9w&libraries=drawing,geometry,places"></script> 
    <script>
    	var opts = {
			center: new google.maps.LatLng(41.229318,0.8649803),
			zoom:16,
			styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
		};

    	var mapa = new google.maps.Map(document.getElementById('mapa'),opts);    	
    	var mark = new google.maps.Marker({
    		position:new google.maps.LatLng(41.229318,0.8649803),
    		title:'Masdenblei',
    		icon:'<?= base_url() ?>img/map-marker.png',
    		map:mapa
    	});
    </script>
	
	<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>