<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Idiomas extends Panel{
        protected $types = array(
            'es'=>'spanish',
            'en'=>'english',
            'fr'=>'french',
            'ru'=>'russian',
            'it'=>'italian',
            'ca'=>'catalan',
            'ch'=>'zh_cn',
            'al'=>'german'
        );
        protected $suffix = 'web_lang';
        function __construct() {
            parent::__construct();
        } 
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }
        function idiomas($x = ''){
            if(empty($x)){
                if(empty($_GET['idioma'])){
                    $this->loadView(array(
                        'crud'=>'user',
                        'output'=>$this->load->view('idiomas',array(),TRUE),
                        'view'=>'panel'
                    ));
                }else{
                    redirect('paginas/idiomas/idiomas/'.$_GET['idioma'].'/');
                }
            }else{
                $crud = $this->crud_function("","");
                $crud->set_subject('idiomas en '.$x);
                $crud->set_clone();
                $crud->where('idioma',$x)
                     ->field_type('idioma','hidden',$x)
                     ->field_type('texto','editor',array('type'=>'textarea'));
                if($crud->getParameters()=='add'){
                    $crud->set_rules('identificador','Identificador','required|is_unique[idiomas.identificador]');
                }
                $crud->callback_column('texto',function($val){
                    return htmlentities($val);
                });
                $crud->callback_after_insert(array($this,'refresh_file'));
                $crud->callback_after_update(array($this,'refresh_file'));
                $crud = $crud->render();
                $this->loadView($crud);
            }
        }

        function refresh_file($post,$primary){
            $texts = $this->db->get_where('idiomas',array('idioma'=>$post['idioma']));
            if($texts->num_rows()>0){
                //Generate
                $file = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ";
                foreach($texts->result() as $t){
                    $t->identificador = str_replace("'",'&#039',$t->identificador);
                    $t->texto = str_replace("'",'&#039',$t->texto);
                    $file.= '$lang[\''.$t->identificador.'\'] = \''.$t->texto.'\';';
                }

                $path = APPPATH.'language/'.$this->types[$post['idioma']].'/'.$this->suffix.'.php';
                file_put_contents($path,$file);
            }            
        } 

        function regenerate(){
            $this->refresh_file(array('idioma'=>'es'),0);
            $this->refresh_file(array('idioma'=>'ca'),0);
            $this->refresh_file(array('idioma'=>'en'),0);
            $this->refresh_file(array('idioma'=>'fr'),0);
            $this->refresh_file(array('idioma'=>'it'),0);
            $this->refresh_file(array('idioma'=>'al'),0);
            $this->refresh_file(array('idioma'=>'ru'),0);
            $this->refresh_file(array('idioma'=>'ch'),0);
        }

        function copiar($origen,$destino){
            $texts = $this->db->get_where('idiomas',array('idioma'=>$origen));
            $this->db->delete('idiomas',array('idioma'=>$destino));
            $this->db->query('insert into idiomas select NULL,identificador,texto,\''.$destino.'\' from idiomas where idioma = \''.$origen.'\'');
            $texts = $this->db->get_where('idiomas',array('idioma'=>$destino));
            if($texts->num_rows()>0){
                //Generate
                $file = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ";
                foreach($texts->result() as $t){
                    $t->identificador = str_replace("'",'&#039',$t->identificador);
                    $t->texto = str_replace("'",'&#039',$t->texto);
                    $file.= '$lang[\''.$t->identificador.'\'] = \''.$t->texto.'\';';
                }

                $path = APPPATH.'language/'.$this->types[$destino].'/'.$this->suffix.'.php';
                file_put_contents($path,$file);
            }            
        }        
    }
?>
