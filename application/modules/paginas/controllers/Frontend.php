<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }

        public function setPosition($lat,$lng){
            $_SESSION['lat'] = $lat;
            $_SESSION['lng'] = $lng;
            $FranMasCercana = $this->db->query('Select *,getPosition("Lat",mapa) as lat, getPosition("Lng",mapa) as lng FROM franquicias ORDER BY getDistance(41.5856863,1.6178891,mapa) ASC LIMIT 1');
            if($FranMasCercana->num_rows()>0){
                $_SESSION['franquicia'] = $FranMasCercana->row();
            }
            echo json_encode(array('fran'=>$FranMasCercana->row(),'info'=>$this->load->view($this->theme.'footer',array(),TRUE,'paginas')));
        }

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view($theme.$url,array('link'=>$url),TRUE),
                    'link'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){                         
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('telefono','Telefono','required');   
            $this->form_validation->set_rules('politicas','Politicas','required');            
            if($this->form_validation->run()){
                $datos = $_POST;                                    
                    $datos['titulo'] = $this->lang->line('titulo');
                    $datos['asunto2'] = $this->lang->line('asunto2');
                    if(!empty($_POST['extras'])){
                        unset($datos['extras']);
                        $datos['extras'] = '';
                        foreach($_POST['extras'] as $n=>$v){
                            $n = $this->lang->line($n);
                            $datos['extras'].= '<p class="MsoNormal"><span style="font-family: arial, helvetica, sans-serif;" data-mce-style="font-family: arial, helvetica, sans-serif;"><strong><span style="color: #808080;" data-mce-style="color: #808080;">'.$n.':</span></strong> '.$v.'</span></p>';                            
                        }
                    }else{
                        $datos['extras'] = '';
                    }

                    if(empty($_POST['asunto'])){
                        $datos['asunto'] = '';
                    }
                    $this->db->insert('contacto',array(
                        'nombre'=>$_POST['nombre'],
                        'email'=>$_POST['email'],
                        'telefono'=>$_POST['telefono'],                        
                        'mensaje'=>$_POST['extras']['message']
                    ));
                    $email = 'info@masdenblei.com';
                    if(!empty($_POST['to'])){
                        unset($datos['to']);
                        $email = $_POST['to'];
                    }else{
                        $datos['to'] = $email;
                    }
                    $this->enviarcorreo((object)$datos,1,$email);                     
                    $this->enviarcorreo((object)$datos,1,$datos['email']);
                    $_SESSION['msj'] = $this->lang->line('success_response_contact_form');
            }else{                
               $_SESSION['msj'] = 'Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>';
            }

            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
        }
        
        
        function subscribir(){
            $err = 'error';
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('politicas','politicas','required');
            if($this->form_validation->run()){
                
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    $_SESSION['msj2'] = $this->success('Inscripción con éxito');
                    $err = 'success';
                }else{
                    $_SESSION['msj2'] = $this->error('Este correo ya esta registrado');
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo $_SESSION['msj2'];
            unset($_SESSION['msj2']);
            //redirect(base_url().'p/contacte#subscribir');
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }

        function search(){
            $domain = base_url();
            $domain = explode('/',$domain);
            $domain = str_replace('www.','',$domain[2]);
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3A'.$domain.'+'.urlencode($_GET['q']).'&oq=site%3A'.$domain.'+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }

                $result = $_SESSION[$_GET['q']];                
                preg_match_all('@<div class=\"ZINbbc xpd O9g5cc uUPGi\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = explode('<footer>',$resultado);
                $resultado = $resultado[0];
                $resultado = str_replace('/url?q=','',$resultado);
                $resultado = explode('<div class="ZINbbc xpd O9g5cc uUPGi">',$resultado);                
                foreach($resultado as $n=>$r){
                    if(strpos($r,'/search?q=site:')){
                        unset($resultado[$n]);
                        continue;
                    }
                    $resultado[$n] = $r;                    
                    $resultado[$n] = substr($r,0,strpos($r,'&'));
                    $pos = strpos($r,'">')+2;
                    $rr = substr($r,$pos);
                    $pos = strpos($rr,'">');
                    $rr = substr($rr,$pos);                    
                    $resultado[$n].= $rr;
                    $resultado[$n] = '<div class="ZINbbc xpd O9g5cc uUPGi">'.$resultado[$n];
                    $resultado[$n] = utf8_encode($resultado[$n]);
                }
                $resultado = implode('',$resultado);
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }

        function sitemap(){
            ob_start();
            $pages = array(
                base_url(),               
                base_url().'el-celler.html',
                base_url().'les-vinyes.html',
                base_url().'contacte.html',
                base_url().'galeria.html', 
                base_url().'els-vins.html',
                base_url().'distribuidors.html',               
                base_url().'blog',
                base_url().'premis.html',
                base_url().'vins-experimentals.html',
                base_url().'politica-de-privacitat.html'
            );

            //Blogs
            foreach($this->db->get_where('blog',array())->result() as $b){
                $pages[] = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            }
            //Vins
            $lang = $_SESSION['lang'];
            foreach($this->elements->vinos()->result() as $b){                
                $pages[] = $b->link;
            }

            $_SESSION['lang'] = 'es';
            foreach($this->elements->vinos()->result() as $b){                
                $pages[] = $b->link;
            }
            $_SESSION['lang'] = 'en';
            foreach($this->elements->vinos()->result() as $b){                
                $pages[] = $b->link;
            }
            $_SESSION['lang'] = $lang;

            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }
    }
